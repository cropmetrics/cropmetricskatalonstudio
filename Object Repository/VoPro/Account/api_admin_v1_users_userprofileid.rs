<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>api_admin_v1_users_userprofileid</name>
   <tag></tag>
   <elementGuidId>b7826410-d504-4e36-8ae5-6c7ea391d958</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;password&quot;,
      &quot;value&quot;: &quot;${password}&quot;
    },
    {
      &quot;name&quot;: &quot;email&quot;,
      &quot;value&quot;: &quot;${email}&quot;
    },
    {
      &quot;name&quot;: &quot;fullname&quot;,
      &quot;value&quot;: &quot;${fullname}&quot;
    },
    {
      &quot;name&quot;: &quot;phone&quot;,
      &quot;value&quot;: &quot;${phone}&quot;
    },
    {
      &quot;name&quot;: &quot;dealer&quot;,
      &quot;value&quot;: &quot;${dealer}&quot;
    },
    {
      &quot;name&quot;: &quot;addressLine1&quot;,
      &quot;value&quot;: &quot;${addressLine1}&quot;
    },
    {
      &quot;name&quot;: &quot;addressLine2&quot;,
      &quot;value&quot;: &quot;${addressLine2}&quot;
    },
    {
      &quot;name&quot;: &quot;town&quot;,
      &quot;value&quot;: &quot;${town}&quot;
    },
    {
      &quot;name&quot;: &quot;state&quot;,
      &quot;value&quot;: &quot;${state}&quot;
    },
    {
      &quot;name&quot;: &quot;zip&quot;,
      &quot;value&quot;: &quot;${zip}&quot;
    },
    {
      &quot;name&quot;: &quot;country&quot;,
      &quot;value&quot;: &quot;${country}&quot;
    },
    {
      &quot;name&quot;: &quot;roles&quot;,
      &quot;value&quot;: &quot;${roles}&quot;
    },
    {
      &quot;name&quot;: &quot;userProfileId&quot;,
      &quot;value&quot;: &quot;${userprofileid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/admin/v1/users/${userprofileid}?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'4'</defaultValue>
      <description></description>
      <id>af2231d9-3d16-43ee-bde6-bd018c6ef963</id>
      <masked>false</masked>
      <name>userprofileid</name>
   </variables>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6ImFkbWluIiwicm9sZSI6InVzZXIiLCJzdWIiOiJhZG1pbiIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTUzODQ3NDM3LCJuYmYiOjE1NTM4NDM1Mzd9.fniyQEeEllxoo10g8MbQB_vIGK75FZxUQQmFgDT0B8M'</defaultValue>
      <description></description>
      <id>ddbb71b0-ca52-403e-b2ba-380efb4c5667</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>'cropme'</defaultValue>
      <description></description>
      <id>0fc8bd9c-f643-40ce-8e69-3a67633de07a</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <variables>
      <defaultValue>'cropmetricsvo.3updated@gmail.cpm'</defaultValue>
      <description></description>
      <id>61681206-577f-408e-916c-991527578538</id>
      <masked>false</masked>
      <name>email</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>6ac10a96-f46f-4319-98e8-7d7bd73dc8eb</id>
      <masked>false</masked>
      <name>fullname</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>159b657c-118c-418e-800f-330ec0bdc799</id>
      <masked>false</masked>
      <name>phone</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>23765295-ee8c-4689-8f3f-fab7af03155d</id>
      <masked>false</masked>
      <name>dealer</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>6563c75f-04b4-4b67-b378-031616f05998</id>
      <masked>false</masked>
      <name>addressLine1</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>0d4997ec-29a5-4f25-8a9a-c7086ec256ad</id>
      <masked>false</masked>
      <name>addressLine2</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>97dcec5b-09cb-4947-9a2b-553ad62438f2</id>
      <masked>false</masked>
      <name>town</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>c456850d-1a27-47a2-b1ad-73932150e414</id>
      <masked>false</masked>
      <name>state</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>84952413-4a82-49c1-b7d5-050511cde5de</id>
      <masked>false</masked>
      <name>zip</name>
   </variables>
   <variables>
      <defaultValue>'updated'</defaultValue>
      <description></description>
      <id>c8848bf6-367f-4de0-9ca7-c6c7826c5a84</id>
      <masked>false</masked>
      <name>country</name>
   </variables>
   <variables>
      <defaultValue>'Client'</defaultValue>
      <description></description>
      <id>20101a9d-0e29-44fe-92b4-e6a53cce49d4</id>
      <masked>false</masked>
      <name>roles</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

//Verify Response
WS.verifyResponseStatusCode(response, 200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
