<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldPost</name>
   <tag></tag>
   <elementGuidId>51744999-9c81-442f-86b6-f393e056f1b7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;id&quot;,
      &quot;value&quot;: &quot;${id}&quot;
    },
    {
      &quot;name&quot;: &quot;farmId&quot;,
      &quot;value&quot;: &quot;${farmId}&quot;
    },
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;${name}&quot;
    },
    {
      &quot;name&quot;: &quot;isValid&quot;,
      &quot;value&quot;: &quot;${isValid}&quot;
    },
    {
      &quot;name&quot;: &quot;legal&quot;,
      &quot;value&quot;: &quot;${legal}&quot;
    },
    {
      &quot;name&quot;: &quot;addressId&quot;,
      &quot;value&quot;: &quot;${addressId}&quot;
    },
    {
      &quot;name&quot;: &quot;address[Id]&quot;,
      &quot;value&quot;: &quot;${address[Id]}&quot;
    },
    {
      &quot;name&quot;: &quot;address[State]&quot;,
      &quot;value&quot;: &quot;${address[State]}&quot;
    },
    {
      &quot;name&quot;: &quot;address[County]&quot;,
      &quot;value&quot;: &quot;${address[County]}&quot;
    },
    {
      &quot;name&quot;: &quot;address[Country]&quot;,
      &quot;value&quot;: &quot;${address[Country]}&quot;
    },
    {
      &quot;name&quot;: &quot;address[Clone]&quot;,
      &quot;value&quot;: &quot;${address[Clone]}&quot;
    },
    {
      &quot;name&quot;: &quot;wxStationId&quot;,
      &quot;value&quot;: &quot;${wxStationId}&quot;
    },
    {
      &quot;name&quot;: &quot;geometry&quot;,
      &quot;value&quot;: &quot;${geometry}&quot;
    },
    {
      &quot;name&quot;: &quot;hasProFreeByProbe&quot;,
      &quot;value&quot;: &quot;${hasProFreeByProbe}&quot;
    },
    {
      &quot;name&quot;: &quot;hasProFreeByWeatherStation&quot;,
      &quot;value&quot;: &quot;${hasProFreeByWeatherStation}&quot;
    },
    {
      &quot;name&quot;: &quot;hasProFreeByVri&quot;,
      &quot;value&quot;: &quot;${hasProFreeByVri}&quot;
    },
    {
      &quot;name&quot;: &quot;CreatedByUserProfileId&quot;,
      &quot;value&quot;: &quot;${CreatedByUserProfileId}&quot;
    },
    {
      &quot;name&quot;: &quot;LastModifiedByUserProfileId&quot;,
      &quot;value&quot;: &quot;${LastModifiedByUserProfileId}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${accoess_token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/Field/?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>a09371c8-0c47-4ef8-b3d8-d82b12f7f6cc</id>
      <masked>false</masked>
      <name>accoess_token</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>e17fe008-8f88-490b-af98-cb8f5c1a6be5</id>
      <masked>false</masked>
      <name>id</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>976a4e96-6f60-404f-b596-2929a54e1108</id>
      <masked>false</masked>
      <name>farmId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>c45ff315-4653-4ca1-b3f2-088e6ef062a1</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>160c9c81-815c-41a8-9bbf-bc6f39c4fcdd</id>
      <masked>false</masked>
      <name>isValid</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>b89fe33b-e8ee-4830-be0f-557927d6246f</id>
      <masked>false</masked>
      <name>legal</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>7e8ae56d-7176-4ab7-b0b1-f6b348055ba0</id>
      <masked>false</masked>
      <name>addressId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>016cca9c-fd60-46ba-84ea-c632d3f56c43</id>
      <masked>false</masked>
      <name>vaddress[Id]</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>325fbcb8-75c6-49ec-87bd-d445409ff39b</id>
      <masked>false</masked>
      <name>address[State]</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>d66124e4-1957-452a-8473-deffec6ef5a9</id>
      <masked>false</masked>
      <name>address[County]</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>9b0cdbd9-f9b6-4e81-ba49-0946760973bf</id>
      <masked>false</masked>
      <name>address[Country]</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>4bd3a63d-530e-4c1d-9670-33762e671faa</id>
      <masked>false</masked>
      <name>address[Clone]</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>a362ef3b-2d78-4b9e-86ad-55bf7e42550e</id>
      <masked>false</masked>
      <name>wxStationId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>7f6063d0-afe3-4e61-90eb-daa6b49f7ddb</id>
      <masked>false</masked>
      <name>geometry</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>1404bd25-6682-4519-90f1-0317cb4a58d1</id>
      <masked>false</masked>
      <name>hasProFreeByProbe</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>696999e1-afa9-4c0a-8ba9-df88bb91ef60</id>
      <masked>false</masked>
      <name>hasProFreeByWeatherStation</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>224ecc69-eb0e-449a-97ad-811a6ada4709</id>
      <masked>false</masked>
      <name>hasProFreeByVri</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>61bdb983-82f9-4b9b-a022-0b227b167026</id>
      <masked>false</masked>
      <name>CreatedByUserProfileId</name>
   </variables>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>58c81cdd-5efc-4302-ac20-0d9cc1012116</id>
      <masked>false</masked>
      <name>LastModifiedByUserProfileId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
