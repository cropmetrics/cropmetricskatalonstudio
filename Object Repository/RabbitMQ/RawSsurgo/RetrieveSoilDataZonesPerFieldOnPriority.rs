<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>RetrieveSoilDataZonesPerFieldOnPriority</name>
   <tag></tag>
   <elementGuidId>0228a993-dd56-4054-996e-ec9169c183cc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;properties\&quot;: {},\n  \&quot;routing_key\&quot;: \&quot;\&quot;,\n  \&quot;payload\&quot;: \&quot;{\\\&quot;messageType\\\&quot;: [   \\\&quot;urn:message:CropMetrics.Fields.Ingest.Messages.Contracts.Interfaces.SoilDataZones:IRetrieveSoilDataZonesPerFieldOnPriorityQueue\\\&quot;], \\\&quot;message\\\&quot;: { \\\&quot;fieldId\\\&quot;: 12, \\\&quot;messageDateTime\\\&quot;: \\\&quot;2018-11-06T18:14:21.6280894-04:00\\\&quot; },\\\&quot;headers\\\&quot;: {}}\&quot;,\n  \&quot;payload_encoding\&quot;: \&quot;string\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:15672/api/exchanges/%2F/CropMetricsFieldsIngestRawSsurgo_RetrieveSoilDataZonesPerFieldOnPriorityQueue/publish</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'Basic Z3Vlc3Q6Z3Vlc3Q='</defaultValue>
      <description></description>
      <id>9cf5f823-201b-4aad-a1c8-e788439e0ed6</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
