<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>ProbeHealthReadings</name>
   <tag></tag>
   <elementGuidId>a74ff828-de53-430d-bd39-8a59348dfe4d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;properties\&quot;:{},\n  \&quot;routing_key\&quot;:\&quot;\&quot;,\n  \&quot;payload\&quot;:\&quot;{    \\\&quot;messageId\\\&quot;: \\\&quot;19710000-605d-0e9e-bfd4-08d6c1a87530\\\&quot;,    \\\&quot;conversationId\\\&quot;: \\\&quot;19710000-605d-0e9e-76a1-08d6c1a78f03\\\&quot;,    \\\&quot;sourceAddress\\\&quot;: \\\&quot;rabbitmq://localhost/CropMetricsProbesIngestJsonReadings_RequestProbesPerTelemetryIdentifier\\\&quot;,    \\\&quot;destinationAddress\\\&quot;: \\\&quot;rabbitmq://localhost/CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestProbeHealthReadingsUpdatePerProbeId\\\&quot;,    \\\&quot;messageType\\\&quot;: [ \\\&quot;urn:message:CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestProbeHealthReadingsUpdatePerProbeId\\\&quot;    ],    \\\&quot;message\\\&quot;: {      \\\&quot;probeId\\\&quot;: 37,      \\\&quot;dataServiceId\\\&quot;: 1,      \\\&quot;messageDateTime\\\&quot;: \\\&quot;2019-04-15T13:45:02.2055697Z\\\&quot;,      \\\&quot;guid\\\&quot;: \\\&quot;6b52dba5-ea08-4ebb-8bcc-eac36d5348bc\\\&quot;    } }\&quot;,\n  \&quot;payload_encoding\&quot;:\&quot;string\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${authorization}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:15672/api/exchanges/%2F/CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestProbeHealthReadingsUpdatePerProbeId/publish</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'Basic Z3Vlc3Q6Z3Vlc3Q='</defaultValue>
      <description></description>
      <id>9ec57293-2772-4bc5-af50-3017314e9fbe</id>
      <masked>false</masked>
      <name>authorization</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
