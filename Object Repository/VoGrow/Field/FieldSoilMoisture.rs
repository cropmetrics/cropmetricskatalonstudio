<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldSoilMoisture</name>
   <tag></tag>
   <elementGuidId>52ef16ec-0020-4870-8c06-9a0a6fcd099b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields/${field_id}/soil-moisture/${start_time_stamp}/${end_time_stamp} </restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6ImNyb3BtZXRyaWNzLmFnc2Vuc2VAZ21haWwuY29tIiwicm9sZSI6InVzZXIiLCJzdWIiOiJjcm9wbWV0cmljcy5hZ3NlbnNlQGdtYWlsLmNvbSIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTY2MTM4NzE1LCJuYmYiOjE1NjM1NDY0MTV9.E04RGOWapE27UDQC9WMcPIIHou2RRS5K6dnzZcfE9hc'</defaultValue>
      <description></description>
      <id>900a429a-cc18-480f-bb2f-ebfa062f0026</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>32</defaultValue>
      <description></description>
      <id>8d1848ef-7668-4366-9a9c-585712ea46dc</id>
      <masked>false</masked>
      <name>field_id</name>
   </variables>
   <variables>
      <defaultValue>1546300800</defaultValue>
      <description></description>
      <id>f46ae80c-fc1c-485e-b199-24fe1d1c0cb4</id>
      <masked>false</masked>
      <name>start_time_stamp</name>
   </variables>
   <variables>
      <defaultValue>1577750400</defaultValue>
      <description></description>
      <id>8b06b3ae-0e3c-42d5-9c0b-e1fe94e81a44</id>
      <masked>false</masked>
      <name>end_time_stamp</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
