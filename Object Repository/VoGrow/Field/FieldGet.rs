<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldGet</name>
   <tag></tag>
   <elementGuidId>f02c16f6-23d1-4d3a-91f1-6ab36d201a20</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;field\&quot;: {\n    \&quot;name\&quot;: \&quot;fld katalon1 updated\&quot;,\n    \&quot;boundary\&quot;: {\n      \&quot;boundaries\&quot;: [\n        {\n          \&quot;boundary\&quot;: {\n            \&quot;coords\&quot;: [\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14647907\n              },\n              {\n                \&quot;x\&quot;: -101.50049403,\n                \&quot;y\&quot;: 37.14264773\n              },\n              {\n                \&quot;x\&quot;: -101.49723246,\n                \&quot;y\&quot;: 37.1411425\n              },\n              {\n                \&quot;x\&quot;: -101.49019435,\n                \&quot;y\&quot;: 37.14223722\n              },\n              {\n                \&quot;x\&quot;: -101.48933604,\n                \&quot;y\&quot;: 37.14620541\n              },\n              {\n                \&quot;x\&quot;: -101.49294093,\n                \&quot;y\&quot;: 37.14866833\n              },\n              {\n                \&quot;x\&quot;: -101.49568751,\n                \&quot;y\&quot;: 37.14976293\n              }\n            ]\n          }\n        }\n      ]\n    }\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields/${fieldId}?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6Implbm55Lm1hbGRvbmFkby50eDFAZ21haWwuY29tIiwicm9sZSI6InVzZXIiLCJzdWIiOiJqZW5ueS5tYWxkb25hZG8udHgxQGdtYWlsLmNvbSIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTU0OTEwMzA1LCJuYmYiOjE1NTQ5MDY0MDV9.NVZN4vs11ND5sQTGAcYaMT3a_-n1yhZHBXBB1lQJlMY'</defaultValue>
      <description></description>
      <id>ccc4d5b0-16ae-45a7-a3c3-284e27bc9da0</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>'6'</defaultValue>
      <description></description>
      <id>9bac6ccc-bf71-4ff4-96c8-129ec626c378</id>
      <masked>false</masked>
      <name>fieldId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
