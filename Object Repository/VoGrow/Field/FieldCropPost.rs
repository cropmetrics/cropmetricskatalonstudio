<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldCropPost</name>
   <tag></tag>
   <elementGuidId>ebb59eff-2ab8-43ad-be87-b3092861e7f0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;field\&quot;: {\n    \&quot;farm_id\&quot;: 11,\n    \&quot;name\&quot;: \&quot;USA 2 fld prb 54321 no setpoint\&quot;,\n    \&quot;boundary\&quot;: {\n      \&quot;boundaries\&quot;: [\n        {\n          \&quot;boundary\&quot;: {\n            \&quot;coords\&quot;: [\n\t\t\t\t{\&quot;x\&quot;:-93.48017692565918,\&quot;y\&quot;:30.187350810351145},{\&quot;x\&quot;:-93.46678733825684,\&quot;y\&quot;:30.18534763754125},{\&quot;x\&quot;:-93.47107887268066,\&quot;y\&quot;:30.193137525171807},{\&quot;x\&quot;:-93.48017692565918,\&quot;y\&quot;:30.187350810351145}\n            ]\n          }\n        }\n      ]\n    }\n  },\n  \&quot;crop\&quot;: {\n        \&quot;crop_type_id\&quot;: 1,\n        \&quot;crop_variety\&quot;: \&quot;Generic test\&quot;,\n        \&quot;seeding_rate\&quot;: 123456,\n        \&quot;planting_date\&quot;: \&quot;2019-04-18\&quot;,\n        \&quot;relative_maturity_id\&quot;: 10\n    },\n  \&quot;probe\&quot;: {\n    \&quot;data_service_id\&quot;: 1,\n    \&quot;external_probe_id\&quot;: \&quot;54321\&quot;,\n    \&quot;telemetry_id\&quot;: \&quot;54321\&quot;,\n    \&quot;probe_latitude\&quot;: 30.1886119910214,\n    \&quot;probe_longitude\&quot;: -93.47268104553223,\n    \&quot;probe_type\&quot;: 1,\n    \&quot;telemetry_type\&quot;: 1,\n\t\&quot;installer_name\&quot;: \&quot;test\&quot;, \n    \&quot;soil_moisture\&quot;: [\n      {\n        \&quot;depth\&quot;: 1,\n        \&quot;soil_moisture\&quot;: 0\n      },\n      {\n        \&quot;depth\&quot;: 2,\n        \&quot;soil_moisture\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 3,\n        \&quot;soil_moisture\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 4,\n        \&quot;soil_moisture\&quot;: 0\n      }\n    ],\n    \&quot;soil_type\&quot;: [\n      {\n        \&quot;depth\&quot;: 1,\n        \&quot;soil_type\&quot;: 0\n      },\n      {\n        \&quot;depth\&quot;: 2,\n        \&quot;soil_type\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 3,\n        \&quot;soil_type\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 4,\n        \&quot;soil_type\&quot;: 3\n      }\n    ],\n    \&quot;note\&quot;: {\n        \&quot;text\&quot;:\&quot;Probe note test in the fld edition\&quot;\n    }\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6ImNyb3BtZXRyaWNzdm8uYWdlbnRmQGdtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIiwic3ViIjoiY3JvcG1ldHJpY3N2by5hZ2VudGZAZ21haWwuY29tIiwiaXNzIjoiQ3JvcE1ldHJpY3MiLCJhdWQiOiJhbGwiLCJleHAiOjE1NTk5NzI4MDksIm5iZiI6MTU1NzM4MDUwOX0.UYOz1_Ax6FIDB3aAQRfcJLY8m7fH0QNNlGxbwhlguJw'</defaultValue>
      <description></description>
      <id>ccc4d5b0-16ae-45a7-a3c3-284e27bc9da0</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 201)

assertThat(response.getStatusCode()).isEqualTo(201)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
