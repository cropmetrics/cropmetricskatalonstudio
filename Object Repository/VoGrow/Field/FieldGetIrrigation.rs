<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldGetIrrigation</name>
   <tag></tag>
   <elementGuidId>2702215e-9c6e-48c8-bf81-ffd9afad3cc9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields/${field_id}/events/${start_date}/${end_date}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6ImNyb3BtZXRyaWNzdm8uYWdlbnRmQGdtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIiwic3ViIjoiY3JvcG1ldHJpY3N2by5hZ2VudGZAZ21haWwuY29tIiwiaXNzIjoiQ3JvcE1ldHJpY3MiLCJhdWQiOiJhbGwiLCJleHAiOjE1NjM5ODEzNjIsIm5iZiI6MTU2MTM4OTA2Mn0.vzZrV3m5HVl63g2v-aa2zIpilkkh_sP-vMflXgY7oR8'</defaultValue>
      <description></description>
      <id>69f3552d-43ff-4627-8ef6-7d64512df077</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>1</defaultValue>
      <description></description>
      <id>4ed25e02-13c5-48c1-88a0-69179f4d9390</id>
      <masked>false</masked>
      <name>field_id</name>
   </variables>
   <variables>
      <defaultValue>1558051200</defaultValue>
      <description></description>
      <id>eb268e55-e479-4040-b78b-6eca2c882cd8</id>
      <masked>false</masked>
      <name>start_date</name>
   </variables>
   <variables>
      <defaultValue>1568764800</defaultValue>
      <description></description>
      <id>2c8c5604-64e2-4868-ad37-e1e332b74608</id>
      <masked>false</masked>
      <name>end_date</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
