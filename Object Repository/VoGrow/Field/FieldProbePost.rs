<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>FieldProbePost</name>
   <tag></tag>
   <elementGuidId>27ede208-d76e-411d-9834-d6f4c090e9fe</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;field\&quot;: {\n    \&quot;farm_id\&quot;: 1,\n    \&quot;name\&quot;: \&quot;USA 1 fld prb\&quot;,\n    \&quot;boundary\&quot;: {\n      \&quot;boundaries\&quot;: [\n        {\n          \&quot;boundary\&quot;: {\n            \&quot;coords\&quot;: [\n\t\t\t{\&quot;x\&quot;:-106.72943115234375,\&quot;y\&quot;:43.49029208393122},{\&quot;x\&quot;:-106.73904418945312,\&quot;y\&quot;:43.45690646829029},{\&quot;x\&quot;:-106.66557312011717,\&quot;y\&quot;:43.46537919154699},{\&quot;x\&quot;:-106.72943115234375,\&quot;y\&quot;:43.49029208393122}\n            ]\n          }\n        }\n      ]\n    }\n  },\n  \&quot;probe\&quot;: {\n    \&quot;data_service_id\&quot;: 1,\n    \&quot;external_probe_id\&quot;: \&quot;1\&quot;,\n    \&quot;telemetry_id\&quot;: \&quot;101637\&quot;,\n    \&quot;probe_latitude\&quot;: 43.47085924792284,\n    \&quot;probe_longitude\&quot;: -106.71134948730469,\n    \&quot;probe_type\&quot;: 2,\n    \&quot;telemetry_type\&quot;: 1,\n    \&quot;soil_moisture\&quot;: [\n      {\n        \&quot;depth\&quot;: 1,\n        \&quot;soil_moisture\&quot;: 1\n      },\n      {\n        \&quot;depth\&quot;: 2,\n        \&quot;soil_moisture\&quot;: 2\n      },\n      {\n        \&quot;depth\&quot;: 3,\n        \&quot;soil_moisture\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 4,\n        \&quot;soil_moisture\&quot;: 4\n      }\n    ],\n    \&quot;soil_type\&quot;: [\n      {\n        \&quot;depth\&quot;: 1,\n        \&quot;soil_type\&quot;: 1\n      },\n      {\n        \&quot;depth\&quot;: 2,\n        \&quot;soil_type\&quot;: 2\n      },\n      {\n        \&quot;depth\&quot;: 3,\n        \&quot;soil_type\&quot;: 3\n      },\n      {\n        \&quot;depth\&quot;: 4,\n        \&quot;soil_type\&quot;: 4\n      }\n    ],\n    \&quot;note\&quot;: {\n        \&quot;text\&quot;:\&quot;Probe note test in the fld creation\&quot;\n    }\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${access_token}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://localhost:10520/WebAPI/api/mobile/v1/fields?</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6Implbm55Lm1hbGRvbmFkby50eDFAZ21haWwuY29tIiwicm9sZSI6InVzZXIiLCJzdWIiOiJqZW5ueS5tYWxkb25hZG8udHgxQGdtYWlsLmNvbSIsImlzcyI6IkNyb3BNZXRyaWNzIiwiYXVkIjoiYWxsIiwiZXhwIjoxNTU0NDYzNTkzLCJuYmYiOjE1NTQ0NTk2OTN9.z60ggtftp_qBR0kmT91lJZQXPEaOJB0W5ZZz8OPMWeA'</defaultValue>
      <description></description>
      <id>ccc4d5b0-16ae-45a7-a3c3-284e27bc9da0</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()



WS.verifyResponseStatusCode(response, 201)

assertThat(response.getStatusCode()).isEqualTo(201)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
