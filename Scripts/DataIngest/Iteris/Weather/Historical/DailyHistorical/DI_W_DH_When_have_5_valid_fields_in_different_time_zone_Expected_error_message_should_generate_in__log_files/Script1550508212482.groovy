import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*
 Preconditions:
  1. Have at least 1 farm created
  2. Have at least 5 fields  created  in different time zones
 
 Use the next message to run the process in RabbitMQ:
 {
 "messageType": [
 "urn:message:CropMetrics.Fields.Ingest.Messages.Contracts.Interfaces.Weather:IRequestWeatherDailyHistoricalBatchJob",
 "urn:message:CropMetrics.Fields.Ingest.Messages.Contracts.Messages.Weather:RequestWeatherDailyHistoricalBatchJobMessage"
 ],
 "message":
 { "fields": [1,2,3,4,5],
 "messageDateTime": "2018-11-06T18:14:21.6280894-04:00" },
 "headers": {}
 }
 */