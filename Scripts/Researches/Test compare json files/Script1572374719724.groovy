import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent
import org.json.JSONObject as JSONObject

import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter

//////////////

import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

/*
import javax.json.stream;
import java.util.*;

import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

import java.lang.String;
import java.lang.Object;
import org.json.JSONObject;
import org.json.JSONArray;
*/

//import java.util.regex.Matcher;
//import java.util.regex.Pattern; 

///////////////// COMPARACION JSON OBJECTS CON VARIALES 
/*
def expected = "{foo: 'bar', list:[{test: '1'},  {rest: '2'}]}"//si funciona "{a : {a : 2}, b : 3}"
//def expected = "{foo: 'bar', list:[{rest: '2'}, {test: '1'}]}" 
def actual = "{foo: 'bar ', list: [{rest:' 2 '},{test:' 1 '}]} "// si funciona "{b : 3, a : {a : 2}}"

// conversion de string a objeto Json
//JSONObject jsonObjExpected = new JSONObject(expected.replaceAll(" ","").replaceAll("\r\n", "").quoteReplacement("[", "{"));
//JSONObject jsonObjExpected = new JSONObject(expected.replaceAll(" ","").replaceAll("\r\n", ""));
JSONObject jsonObjExpected = new JSONObject(expected.replaceAll("[\\s\\\r\n]","").replace("\\[\\]",""));// reemplaza //s espacios, y \r\n saltos de linea al mismo tiempo
JSONObject jsonObjActual = new JSONObject(actual.replaceAll(" ","").replaceAll("\r\n", ""));

println('expected' +expected) // como string tal cual definido
println( 'jsonObjExpected: '+jsonObjExpected) // como objeto json con dobles comillas en cada variable antes de las :

println('actual' +actual) // como string tal cual definido
println( 'jsonObjActual: '+jsonObjActual)

JSONAssert.assertEquals(jsonObjExpected, jsonObjActual, JSONCompareMode.NON_EXTENSIBLE);   // funciona con las variales sin arreglos internosn, sin considerar variables o valores extras
*/

///////////////////////////////http://jsonassert.skyscreamer.org/javadoc/org/skyscreamer/jsonassert/JSONCompareMode.html
//JSONAssert.assertEquals(jsonObjActual, jsonObjExpected,JSONCompareMode.NON_EXTENSIBLE);
//JSONAssert.assertEquals(jsonObjExpected, jsonObjActual, JSONCompareMode.LENIENT);
//JSONAssert.assertEquals(jsonObjActual, jsonObjExpected,JSONCompareMode.STRICT)
//JSONAssert.assertEquals(jsonObjActual, jsonObjExpected,JSONCompareMode.STRICT_ORDER)
 

 


///////////////// COMPARACION JSON OBJECTS CON ARCHIVOS JSON  
// USAR EL ENDPOINT DE ITERIS WEATHER FORECAST.. CONTRA LO GUARDADO EN LA DB LOCAL 
//def expected = new File("C:\\KatalonFiles\\DailyForecastFldId1.txt").getText('UTF-8');
//def actual = new File("C:\\KatalonFiles\\DailyForecastFldId1_DB.txt").getText('UTF-8');

//def expected = new File("C:\\KatalonFiles\\DailyForecastFldId1_Test2.txt").getText('UTF-8'); 
//def actual = new File("C:\\KatalonFiles\\DailyForecastFldId1_Test2-1.txt").getText('UTF-8');


def expected = new File("C:\\KatalonFiles\\iteris_crophealt_solo_data.txt").getText('UTF-8');
def actual = new File("C:\\KatalonFiles\\DB_crophealt_solo_data.txt").getText('UTF-8');


// conversion de string a objeto Json
JSONObject jsonObjExpected = new JSONObject(expected.replaceAll("\r\n","").replaceAll("\\s",""));
JSONObject jsonObjActual = new JSONObject(actual.replaceAll("\r\n","").replaceAll("\\s",""));

println('expected' +expected) 
println( 'jsonObjExpected: '+jsonObjExpected) 

println('actual' +actual)
println( 'jsonObjActual: '+jsonObjActual)

JSONAssert.assertEquals(jsonObjExpected, jsonObjActual, JSONCompareMode.NON_EXTENSIBLE);   

