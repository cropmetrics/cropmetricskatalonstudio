import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


//import org.mockserver.model;
import org.mockserver.integration.ClientAndServer;

//import org.mockserver.integration;

ClientAndServer clientServer = ClientAndServer.startClientAndServer(2018);
	 println('Mock server status : ' + clientServer.isRunning());      //running status: true
/*		 
// simple expectation for GET
		 clientServer.when(new HttpRequest().withMethod("GET"))
				 .respond(new HttpResponse().withStatusCode(HttpStatusCode.OK_200.code())
						 .withBody("{'message': 'How may I help you?'}"));

					 
	/*	 
// simple expectation for POST
clientServer.when(new HttpRequest().withMethod("POST").withPath("/start"))
				 .respond(new HttpResponse().withStatusCode(HttpStatusCode.ACCEPTED_202.code())
						 .withBody("Successful");
*/

test = WS.sendRequest(findTestObject('GET mock server'))

WS.verifyResponseStatusCode(test, 200)

// Post token
//responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def testResult = slurper.parseText(test.getResponseBodyContent())

// Get result parameters of token
//def MessageResult = testResult.message

println('MessageResult: ' + MessageResult)