import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Take note of the number of the decimals for the salinity per sensor, should be 1
// serial probe = 79810 , with only 1 valid reading in the current day

def probeSerial = '79810'
def probeId = '37'

// Connection to vo_agSenseProbeRawReadings dtabaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_agSenseProbeRawReadings', '5432', 'postgres', 'admin')

// Print salinity values from the vo_agSenseProbeRawReadings daatabase
def query = "select date(readingdatetime),to_char(readingdatetime,'HH24:MI:SS.US') as hour,round (sal1::numeric,1) as sal1,round(sal2::numeric,1) as sal2,round(sal3::numeric,1) as sal3,round(sal4::numeric,1) as sal4,round(sal5::numeric,1) as sal5,round(sal6::numeric,1) as sal6,round(sal7::numeric,1) as sal7,round(sal8::numeric,1) as sal8,round(sal9::numeric,1)as sal9,round(sal10::numeric,1)as sal10,round(sal11::numeric,1)as sal11,round(sal12::numeric,1)as sal12 from agsenseprobereading where serial = '" + probeSerial +"'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def readingDate= queryResult.getString(1)
	def readingTime= queryResult.getString(2)
	def Sal1 = queryResult.getString(3)
	def Sal2 = queryResult.getString(4)
	def Sal3 = queryResult.getString(5)
	def Sal4 = queryResult.getString(6)
	def Sal5 = queryResult.getString(7)
	def Sal6 = queryResult.getString(8)
	def Sal7 = queryResult.getString(9)
	def Sal8 = queryResult.getString(10)
	def Sal9 = queryResult.getString(11)
	def Sal10 = queryResult.getString(12)
	def Sal11 = queryResult.getString(13)
	def Sal12 = queryResult.getString(14)
	
	println('The readingDateTime is :' + readingDate + 'T' + readingTime)
	println('The Sal1 is :' + Sal1)
	println('The Sal2 is :' + Sal2)
	println('The Sal3 is :' + Sal3)
	println('The Sal4 is :' + Sal4)
	println('The Sal5 is :' + Sal5)
	println('The Sal6 is :' + Sal6)
	println('The Sal7 is :' + Sal7)
	println('The Sal8 is :' + Sal8)
	println('The Sal9 is :' + Sal9)
	println('The Sal10 is :' + Sal10)
	println('The Sal11 is :' + Sal11)
	println('The Sal12 is :' + Sal12)
	
	GlobalVariable.AgSenseReadings = '[{"s1": ' + Sal1 + ', "s2": ' + Sal2 + ', "s3": ' + Sal3 + ', "s4": ' + Sal4 + ', "s5": ' + Sal5 + ', "s6": ' + Sal6 + ', "s7": ' + Sal7 + ', "s8": ' + Sal8 + ', "s9": ' + Sal9 + ', "s10": ' + Sal10 + ', "s11": ' + Sal11 + ', "s12": ' + Sal12 + ', "readingDateTime": "' + readingDate + 'T' + readingTime + '"}]'
	
}

println(GlobalVariable.AgSenseReadings)

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();

// Connection to vo_probeReadings databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.salinity table
def query2 = "select readings from salinity where probe_id = '" + probeId +"'"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def readings= queryResult2.getString(1)
		
	println('The readings is :' + readings)
	WS.verifyEqual(GlobalVariable.AgSenseReadings, readings) 
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();

