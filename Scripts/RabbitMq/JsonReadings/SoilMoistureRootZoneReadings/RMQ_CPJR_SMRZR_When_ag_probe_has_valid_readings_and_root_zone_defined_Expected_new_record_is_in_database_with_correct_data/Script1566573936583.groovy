import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Take note of the number of the decimals for the sensors, should be 8
// Serial probe = 79810 , with only 1 valid reading in the current day
// Take note of the root depth defined is 66 or more to have the same values than the soil moisture readings

def probeId = '37' // to be changed

// Connection to vo_probeReadings databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.soilmoisture table
def query = "select readings from soilmoisture where probe_id = '" + probeId +"'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	GlobalVariable.AgSenseReadings= queryResult.getString(1)
}

// Print new record created in vo_probeReadings.soilmoisturerootzone table
def query2 = "select readings from soilmoisturerootzone where probe_id = '" + probeId +"'"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def rootZoneReadings= queryResult2.getString(1)
	WS.verifyEqual(GlobalVariable.AgSenseReadings, rootZoneReadings)
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();

