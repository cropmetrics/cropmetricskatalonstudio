import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

import org.json.JSONObject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/*PReconditions
 1. Have at least 1 agsense probe with valid readings into the Raw Readings database
 2. Have at least a valid user with the probe of the precondition 1 assigned in their account
 3. Have at least one root zone defined in the probe of the precondition 2, as depth value 66 to have the same reassingd than the soilmoisture table
 */

def authorization = GlobalVariable.authorizationRabbitMq

DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

// Setting up the RabbitMQ exchange request
def exchangeRequest = ((findTestObject('RabbitMQ/JsonReadings/SoilMoistureRootZoneReadings', [('authorization') : authorization])) as RequestObject)
def exchangeContractName = 'CropMetrics.Probes.Ingest.Messages.Contracts.Interfaces:IRequestSoilMoistureRootZoneReadingsUpdatePerProbeId'

// Creating the message body to test the RabbitMQ exchange
JSONObject exchangeMessage = new JSONObject();
exchangeMessage.put("probeId", 37);
exchangeMessage.put("dataServiceId", 1);
exchangeMessage.put("messageDateTime", LocalDateTime.now().format(formatter));

JSONObject exchangeContract = new JSONObject();
exchangeContract.put("messageType", ["urn:message:" + exchangeContractName]);
exchangeContract.put("message", exchangeMessage)

JSONObject exchangeBody = new JSONObject();
exchangeBody.put("properties", new JSONObject());
exchangeBody.put("routing_key", "");
exchangeBody.put("payload", exchangeContract.toString());
exchangeBody.put("payload_encoding", "string");

String bodyRequest = exchangeBody.toString();

println('Exchange message body request is: ' + bodyRequest)

// Sending the message to the RabbitMQ exchange
try {
	exchangeRequest.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

// Get body response
def exchangeResponse = WS.sendRequest(exchangeRequest)

// Get status code
WS.verifyResponseStatusCode(exchangeResponse, 200)

