import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Take note of the number of the decimals for the sensors, should be 8
// serial probe = 79810 , with only 1 valid reading in the current day

def probeSerial = '79810'
def probeId = '37' // to be changed
 
// Connection to vo_agSenseProbeRawReadings dtabaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_agSenseProbeRawReadings', '5432', 'postgres', 'admin')

// Print the values from the vo_agSenseProbeRawReadings database
def query = "select date(readingdatetime),to_char(readingdatetime,'HH24:MI:SS.US') as hour,round((s1/25.4)::numeric,8) as s1,round((s2/25.4)::numeric,8) as s2,round((s3/25.4)::numeric,8) as s3,round((s4/25.4)::numeric,8) as s4,round((s5/25.4)::numeric,8) as s5,round((s6/25.4)::numeric,8) as s6,round((s7/25.4)::numeric,8) as s7,round((s8/25.4)::numeric,8) as s8,round((s9/25.4)::numeric,8)as s9,round((s10/25.4)::numeric,8)as s10,round((s11/25.4)::numeric,8)as s11,round((s12/25.4)::numeric,8)as s12 from agsenseprobereading where serial = '" + probeSerial +"'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def readingDate= queryResult.getString(1)
	def readingTime= queryResult.getString(2)
	def S1 = queryResult.getString(3)
	def S2 = queryResult.getString(4)
	def S3 = queryResult.getString(5)
	def S4 = queryResult.getString(6)
	def S5 = queryResult.getString(7)
	def S6 = queryResult.getString(8)
	def S7 = queryResult.getString(9)
	def S8 = queryResult.getString(10)
	def S9 = queryResult.getString(11)
	def S10 = queryResult.getString(12)
	def S11 = queryResult.getString(13)
	def S12 = queryResult.getString(14)
	
	println('The readingDateTime is :' + readingDate + 'T' + readingTime)
	println('The SM1 is :' + S1)
	println('The SM2 is :' + S2)
	println('The SM3 is :' + S3)
	println('The SM4 is :' + S4)
	println('The SM5 is :' + S5)
	println('The SM6 is :' + S6)
	println('The SM7 is :' + S7)
	println('The SM8 is :' + S8)
	println('The SM9 is :' + S9)
	println('The SM10 is :' + S10)
	println('The SM11 is :' + S11)
	println('The SM12 is :' + S12)
	
	GlobalVariable.AgSenseReadings = '[{"s1": ' + S1 + ', "s2": ' + S2 + ', "s3": ' + S3 + ', "s4": ' + S4 + ', "s5": ' + S5 + ', "s6": ' + S6 + ', "s7": ' + S7 + ', "s8": ' + S8 + ', "s9": ' + S9 + ', "s10": ' + S10 + ', "s11": ' + S11 + ', "s12": ' + S12 + ', "readingDateTime": "' + readingDate + 'T' + readingTime + '"}]'
	
}

println(GlobalVariable.AgSenseReadings)

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();

// Connection to vo_probeReadings databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_probeReadings', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.soilmoisture table
def query2 = "select readings from soilmoisture where probe_id = '" + probeId +"'"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def readings= queryResult2.getString(1)
		
	println('The readings is :' + readings)
	WS.verifyEqual(GlobalVariable.AgSenseReadings, readings) // problem when the last deciaml is 0 in the query of vo_agSenseProbeRawReadings database, because in vo_probeReadings is saved without last deciaml
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'();




