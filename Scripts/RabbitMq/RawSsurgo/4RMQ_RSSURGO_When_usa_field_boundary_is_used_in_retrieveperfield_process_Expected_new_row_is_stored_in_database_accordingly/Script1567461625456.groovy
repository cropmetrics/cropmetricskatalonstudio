import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


def FieldId = '12' // to be changed

// Connection to vo_ssurgoRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_ssurgoRawData', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.soilmoisture table
def query = 'select zones from spatial.soildatawgs84geographic where field_id = ' + FieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

def ssurgoZones = null
while (queryResult.next()) {
	zones = queryResult.getString(1);
	ssurgoZones=zones;
	
}
 if(ssurgoZones != null)
 {
	 println('SSURGO zones stored in DB for fieldId ' + FieldId + ' are: ' + ssurgoZones);
 }
 
 else
	{
		assert(ssurgoZones.isEmpty())
	}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()