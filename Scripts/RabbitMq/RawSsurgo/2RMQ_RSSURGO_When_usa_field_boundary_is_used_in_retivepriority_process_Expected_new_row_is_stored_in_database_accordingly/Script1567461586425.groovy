import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import java.io.BufferedReader as BufferedReader
import java.io.FileReader as FileReader
import java.io.IOException as IOException


// Have at least 1 field added in USA, e.g. fieldId of the FieldName: 'abFisher Home' where tieZoneIana is America/Chicago
/*
 * Use the next SSURGO endpoint (BBOX filter) https://SDMDataAccess.sc.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs to get the value stores in the database with the next steps>
 * https://SDMDataAccess.sc.egov.usda.gov/Spatial/SDMWGS84Geographic.wfs?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=MapunitPoly&FILTER=<Filter><BBOX><PropertyName>Geometry</PropertyName><Box srsName='EPSG:4326'><coordinates>-101.388977,39.788228 -101.376505,39.797743</coordinates></Box></BBOX></Filter>&SRSNAME=EPSG:4326&OUTPUTFORMAT=GML2
 * Where <coordinates>{(MINX, MINY) (MAXX, MAXY)}</coordinates>  
 *  Take note that the bbox = min Longitude (-, x), min Latitude (+, y), max Longitude , max Latitude ; The first and third values of the next query
 *  bbox = (MINX, MINY) (MAXX, MAXY) ; [first pair and third pair] NOTE: the response body should be save as txt and converte to geoJson with the next site: https://ogre.adc4gis.com/
 *  Filed obtined should be compare with json from ssurgo and json of the row store in the database using the next site: http://www.jsondiff.com/
 *  of the result of the next QUERY in virtualoptimizerdatabase: select st_envelope(geometry) from cropmetrics.field where id = 21 
 *  to get: geo coordinates as ((MINX, MINY) (MINX, MAXY) (MAXX, MAXY) (MAXX, MINY) (MINX, MINY))


 */

// read the file content with json zone values

//def cadena='';
//def xadena2 = null;

////////////////////////
/*
def FieldId = '21' // to be changed

// Connection to vo_ssurgoRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_ssurgoRawData', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.soilmoisture table
def ssurgoZones = null
def query = 'select zones from spatial.soildatawgs84geographic where field_id = ' + FieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {	
	zones = queryResult.getString(1);
	ssurgoZones = zones.replaceAll(" ","");
	println('SSURGO FIELD Zones: ' + ssurgoZones);
	
	//def content = new File("C:\\KatalonFiles\\ABHomeSSURGO.txt").getText('UTF-8')
	def content = new File("C:\\KatalonFiles\\Fld21_Json.txt").getText('UTF-8')
	def FileContent = content.replaceAll(" ","");
	println('text content: ' + FileContent)

	//WS.verifyEqual(FileContent,ssurgoZones)
	
}



//println('CAdena 2VALUE OF THE FIELD: ' + xadena2);

//String [] lines = new File('C:\\KatalonFiles\\ABHomeSSURGO.txt') as String[]
//String content = new File("C:\\KatalonFiles\\ABHomeSSURGO.txt").getText('utf-8')



//println cadena2.equals(ssurgoZones);
//println cadena2.compareTo(ssurgoZones);
//WS.verifyEqual(content,ssurgoZones)

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()
 // comparador semantico, comoparacion de json data. hacer el plarsing con la libreira.., o reemplazar espacion mediante una funcion

*/
//////////////////////////////////////////////////////////// uploaded tu the server git lab

def FieldId = '21' // to be changed

// Connection to vo_ssurgoRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'vo_ssurgoRawData', '5432', 'postgres', 'admin')

// Print new record created in vo_probeReadings.soilmoisture table
def query = 'select zones from spatial.soildatawgs84geographic where field_id = ' + FieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

def ssurgoZones = null
while (queryResult.next()) {
	zones = queryResult.getString(1);
	ssurgoZones=zones;
	
}

 if(ssurgoZones != null)
 {
	 println('SSURGO zones stored in DB for fieldId ' + FieldId + ' are: ' + ssurgoZones);
 }
 
 else
	{
		assert(ssurgoZones.isEmpty())
	}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()

