import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent
import org.json.JSONObject as JSONObject
import java.time.LocalDateTime as LocalDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter

/*PReconditions
 1. Have at least 1 field created
 */

/*
def authorization = GlobalVariable.authorizationRabbitMq

DateTimeFormatter formatter = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss')

// Setting up the RabbitMQ exchange request
def exchangeRequest = ((findTestObject('RabbitMQ/WeatherIteris/DailyForecast', [('authorization') : authorization])) as RequestObject)
def exchangeContractName = 'CropMetrics.Fields.Ingest.Messages.Contracts.Interfaces.Weather:IRequestWeatherDailyForecastBatchJob'

// Creating the message body to test the RabbitMQ exchange
JSONObject exchangeMessage = new JSONObject()
exchangeMessage.put('fields', [1])
exchangeMessage.put('messageDateTime', LocalDateTime.now().format(formatter))

JSONObject exchangeContract = new JSONObject()
exchangeContract.put('messageType', ['urn:message:' + exchangeContractName])
exchangeContract.put('message', exchangeMessage)

JSONObject exchangeBody = new JSONObject()
exchangeBody.put('properties', new JSONObject())
exchangeBody.put('routing_key', '')
exchangeBody.put('payload', exchangeContract.toString())
exchangeBody.put('payload_encoding', 'string')

String bodyRequest = exchangeBody.toString()

println('Exchange message body request is: ' + bodyRequest)

// Sending the message to the RabbitMQ exchange
try {
	exchangeRequest.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

// Get body response
def exchangeResponse = WS.sendRequest(exchangeRequest)

// Get status code
WS.verifyResponseStatusCode(exchangeResponse, 200)

*/
///////////////////////////////////////////// CONNECTION DAT5ABASE


// connection DB
// Use the next iteris endpoint, take note the planting date of the growth in the field to start with start_time
// Current and Forecast Weather with Daily Forecast - v1.1:
// https://ag.us.clearapis.com/v1.1/forecast/daily?app_id={{APP_ID}}&app_key={{APP_KEY}}&start=0&end=7&location=43.4802556014456,-106.701990735865&unitcode=si-std-precise

def FieldId = '1' // to be changed

// Connection to iterisRawData databaase
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'iterisRawData', '5432', 'postgres', 'admin')

// Print new record created in iterisRawData db
def query = 'select values from weather.dailyforecast where field_id = ' + FieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

def valuesIteris = new File("C:\\KatalonFiles\\DailyForecastFldId1.txt").getText('UTF-8').replaceAll(" ","").replaceAll("\r\n", "").replaceAll('[', "{").replaceAll(']', "}");

def values = 1

while (queryResult.next()) {
	values = queryResult.getString(1).replaceAll(" ","").replaceAll("\r\n", "").replaceAll("[", "{").replaceAll("]", "}");
	
	if(values != null)
	{
		println('Values for daily forecast for field id ' + FieldId + ' is: ' + values);
	}
	else
	{
		assert(values.isEmpty())
	}
}

CustomKeywords.'connectionCmDatabase.CmConnection.closeDatabaseConnection'()

