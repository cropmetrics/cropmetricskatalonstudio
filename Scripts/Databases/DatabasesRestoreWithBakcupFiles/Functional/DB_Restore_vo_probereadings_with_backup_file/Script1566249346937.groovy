import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.io.BufferedReader;
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.nio.file.Paths

/*
//Precondition
1. Have the vo_probeReadings database created
2. Have backup file of the previous database to restore , take note of the path file
*/

//Define variables for db connection
def server = GlobalVariable.server
def port = GlobalVariable.port
def user = GlobalVariable.userDb
def password = GlobalVariable.passwordDb
def pathPostgreSQL = GlobalVariable.pathPostgreSQLBin
def database = 'vo_probeReadings'
def pathDumpFile = Paths.get(GlobalVariable.pathDumpFiles, database + '.dump')

// Create the command to run
String cRestore = String.format('cd \"%s\" && pg_restore -U %s -h %s -p %d -c -C -d %s -v \"%s\"',
	pathPostgreSQL,
	user, 
	server,
	port,
	database,
	pathDumpFile);

// Run the command line
CustomKeywords.'commandLine.cmd.cmdLine'(cRestore, password)

 println ('The restore ' + database + ' database was completed...')


