import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*
 //Precondition
 1. Have the vo_sentekProbeReadings database created
 2. Have the sql file of the previous database to restore , take note of the path file
 */

//Define variables for db connection
def server = GlobalVariable.server
def port = GlobalVariable.port
def user = GlobalVariable.userDb
def password = GlobalVariable.passwordDb
def pathPostgreSQL = GlobalVariable.pathPostgreSQLBin
def database = 'vo_sentekProbeReadings'
def pathSqlFile = 'C:\\KatalonFiles\\sentekProbeReading.Data.Location.sql'

// Create the command to run the vo_sentekProbeReadings database
String cRestore = String.format("cd \"%s\" && psql -U %s -d %s -a -f \"%s\"",pathPostgreSQL,user,database,pathSqlFile);


// Run the command line
CustomKeywords.'commandLine.cmd.cmdLine'(cRestore, password)

println ('The restore vo_sentekProbeReadings database data was completed...')
