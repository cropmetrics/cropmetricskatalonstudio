import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB to print the new userprofile Id
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

println('new account with the userprofile_id is : ' + GlobalVariable.userId)

def query = 'select username from cropmetrics.userprofile where id = ' + GlobalVariable.userId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)


while (queryResult.next())
{
	def usernameAdded = queryResult.getString(1)

	println('new account added with the next username is :' + usernameAdded)
	
}

//Print the usrId of the new userprofile using the user table
def query1 = 'select id from cropmetrics.user where userprofile_id = ' + GlobalVariable.userId
def queryResult1 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query1)

while (queryResult1.next())
{
	def userIdAdded = queryResult1.getString(1)

	println('new account added have the next user_id in the user table :' + userIdAdded)
	
}

//Print the new userroleId in userrole table of the new account created
def query2 = 'select ur.id  from cropmetrics.userprofile upp inner join cropmetrics.user u  on u.userprofile_id=upp.id inner join cropmetrics.userrole ur  on ur.user_id = u.id where upp.id =  ' + GlobalVariable.userId
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query2)

while (queryResult2.next())
{
	def userRoleAdded = queryResult2.getString(1)

	println('new account added has the next userRoleId :' + userRoleAdded)
	
}

//Print the new rolename of the new account created
def query3 = 'select r.name  from cropmetrics.userprofile upp inner join cropmetrics.user u  on u.userprofile_id=upp.id inner join cropmetrics.userrole ur  on ur.user_id = u.id inner join cropmetrics.role r on r.id = ur.role_id where upp.id = ' + GlobalVariable.userId + 'and ur.role_id=3'
def queryResult3 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query3)

while (queryResult3.next())
{
	def userRoleNameAdded = queryResult3.getString(1)

	println('new account added has the next role name :' + userRoleNameAdded)
	
}

//Print the new user season in userprofileseason table of the new account created
def query4 = 'select s.name from cropmetrics.userprofileseason ups inner join cropmetrics.season s on s.id = ups.season_id where ups.userprofile_id =  ' + GlobalVariable.userId
def queryResult4 = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query4)

while (queryResult4.next())
{
	def userSeasonAdded = queryResult4.getString(1)

	println('New account added has the next season name assigned :' + userSeasonAdded)
}