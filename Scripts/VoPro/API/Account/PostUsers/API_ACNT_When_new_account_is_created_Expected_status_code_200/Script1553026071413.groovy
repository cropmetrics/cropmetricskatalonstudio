import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//  Define attribute values by variables
//def username = username
def password = password
//def confirmPassword = confirmPassword
def email = email
def fullname = fullname
def phone = phone
def dealer = dealer
def addressLine1 = addressLine1
def addressLine2 = addressLine2
def town = town
def state = state
def zip = zip
def country = country
def roles = roles
//def modifierUsername = modifierUsername
//def modifierUserId = modifierUserId

// Get token
def tokenUserName = 'admin'
def tokenPassword = 'cropmetricsvo'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get responseToken body response attributes
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. current access token generated: ' + accessToken)

// new endpoint
responseAdminRegister = WS.sendRequest(findTestObject('VoPro/Account/api_admin_v1_users', [('access_token') : accessToken, ('password') : password, ('email') : email
	, ('fullname') : fullname, ('phone') : phone, ('dealer') : dealer, ('addressLine1') : addressLine1, ('addressLine2') : addressLine2, ('town') : town
	, ('state') : state, ('zip') : zip, ('country') : country, ('roles') : roles]))

WS.verifyResponseStatusCode(responseAdminRegister, 200)

// Get body response attributes for AdminRegister endpoint
def slurper1 = new groovy.json.JsonSlurper()
def AdminRegisterResult = slurper1.parseText(responseAdminRegister.getResponseBodyContent())

GlobalVariable.userId = AdminRegisterResult.id

println('New account created has the id: ' + GlobalVariable.userId)

