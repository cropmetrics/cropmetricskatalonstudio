import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

// Define attribute values
def userprofileid = '42'
def password = 'cropmetrics'
def email = '!!!!!!!'
def fullname = 'test validation'
def phone = '123'
def dealer = 'x'
def addressLine1 = 'address line 1 test validation'
def addressLine2 = 'address line 2 test validation'
def town = 'townValidation'
def state = 'stateValidation'
def zip = 'xx'
def country = 'xx'
def roles = 'Client'

// Get token
def tokenUserName = 'admin'
def tokenPassword = 'cropmetricsvo'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get responseToken body response attributes
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('The access token generated for the user ' + tokenUserName + ' is : ' + accessToken)

//Put endpoint to update the acocunt id
responsePutUser = WS.sendRequest(findTestObject('VoPro/Account/api_admin_v1_users_userprofileid', [('userprofileid') : userprofileid, ('access_token') : accessToken
			, ('password') : password, ('email') : email, ('fullname') : fullname, ('phone') : phone
			, ('dealer') : dealer, ('addressLine1') : addressLine1, ('addressLine2') : addressLine2, ('town') : town, ('state') : state
			, ('zip') : zip, ('country') : country, ('roles') : roles]))

WS.verifyResponseStatusCode(responsePutUser, 400)

// Get response message for AdminRegister endpoint
def slurper1 = new groovy.json.JsonSlurper()
def putResult = slurper1.parseText(responsePutUser.getResponseBodyContent())
def messageResponse = putResult.message
def messageExpected = "The request is invalid. The email is not a valid email address. " + email

//Print message response
println('Error message response: ' + messageResponse)
println('Error message expected: ' + messageExpected)

WS.verifyElementPropertyValue(responsePutUser, 'message', messageExpected)
