import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent
import groovy.json.JsonSlurper as JsonSlurper

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm and field created in the account of the precondition 1
// 3. Have at least one probe assigned to the account of the precondition 1, take note that the probe must have onlly 1 setpoint defined in the current year

// Variables hardcoded for fieldId of the precondition 2 , to verify the body response of the endpoint
def fieldId = 1
def unit = 'inches'
def fieldName = 'USA 1 fld prb 38058 with corn'
def farmName = 'GrowerOnlyFarm'
def userName = 'grower only'
def cropType = 'Corn'
def cropVariety = 'Generic'
def cropRelativeMaturity = '70'
def cropPlantingPopulation= 123456
def cropPlantingDate = '2019-09-02T00:00:00'
def techType = 'Capacitance Probe Scheduling'
def AWCRootZone = 1.32

// Get token for the user of the precondition 1
def tokenUserName = 'cropmetrics.agentrw@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('Token generated is: ' + accessToken)

//Get field endpoint
requestGet = WS.sendRequest(findTestObject('VoGrow/Report/SeasonSummaryReport', [('access_token') : accessToken, ('field_id') : fieldId]))

// Get body response attributes for put field endpoint

def slurper12 = new groovy.json.JsonSlurper()
def responseTESTCOMPARE = slurper12.parseText(requestGet.getResponseBodyContent())

//Print bosy response
println('The field has:  ' + responseTESTCOMPARE.metadata.units.available_water_capacity_root_zone)
println('The field has:  ' + responseTESTCOMPARE.metadata.units.event_rate)
println('The field has:  ' + responseTESTCOMPARE.metadata.units.evapotranspiration)
println('The field has:  ' + responseTESTCOMPARE.details.name)
println('The field has:  ' + responseTESTCOMPARE.details.farm)
println('The field has:  ' + responseTESTCOMPARE.details.user)
println('The field has:  ' + responseTESTCOMPARE.details.crop.type)
println('The field has:  ' + responseTESTCOMPARE.details.crop.variety)
println('The field has:  ' + responseTESTCOMPARE.details.crop.relative_maturity)
println('The field has:  ' + responseTESTCOMPARE.details.crop.planting_date)
println('The field has:  ' + responseTESTCOMPARE.details.crop.planting_population)
println('The field has:  ' + responseTESTCOMPARE.details.technology_type)
println('The field has:  ' + responseTESTCOMPARE.details.soil_type)
println('The field has:  ' + responseTESTCOMPARE.details.available_water_capacity_root_zone)
println('The field has:  ' + responseTESTCOMPARE.details.irrigation_type)
println('The field has:  ' + responseTESTCOMPARE.details.gpm)
println('The field has:  ' + responseTESTCOMPARE.details.fsa_track)


// Verify body response with the dabatabase data
WS.verifyElementPropertyValue(requestGet, 'metadata.units.available_water_capacity_root_zone', unit)
WS.verifyElementPropertyValue(requestGet, 'metadata.units.event_rate', unit)
WS.verifyElementPropertyValue(requestGet, 'metadata.units.evapotranspiration', unit)
WS.verifyElementPropertyValue(requestGet, 'details.name', fieldName)
WS.verifyElementPropertyValue(requestGet, 'details.farm', farmName)
WS.verifyElementPropertyValue(requestGet, 'details.user', userName)
WS.verifyElementPropertyValue(requestGet, 'details.crop.type', cropType)
WS.verifyElementPropertyValue(requestGet, 'details.crop.variety', cropVariety)
WS.verifyElementPropertyValue(requestGet, 'details.crop.relative_maturity', cropRelativeMaturity)
WS.verifyElementPropertyValue(requestGet, 'details.crop.planting_date', cropPlantingDate)
WS.verifyElementPropertyValue(requestGet, 'details.crop.planting_population', cropPlantingPopulation)
WS.verifyElementPropertyValue(requestGet, 'details.technology_type', techType)
WS.verifyElementPropertyValue(requestGet, 'details.soil_type', 'null')
WS.verifyElementPropertyValue(requestGet, 'details.available_water_capacity_root_zone', AWCRootZone)
WS.verifyElementPropertyValue(requestGet, 'details.irrigation_type', 'null')
WS.verifyElementPropertyValue(requestGet, 'details.gpm', 'null')
WS.verifyElementPropertyValue(requestGet, 'details.fsa_track', 'null')

