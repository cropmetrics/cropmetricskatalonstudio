import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as grower

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)


// Get user
responseUser = WS.sendRequest(findTestObject('VoGrow/User/User', [('access_token') : accessToken]))

// Get status code
WS.verifyResponseStatusCode(responseUser, 200)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
//def responseBodyField = slurper1.parseText(responseField.getResponseBodyContent())
def responseBodyUser = slurper1.parseText(responseUser.getResponseBodyContent())

//Get response values
def id = responseBodyUser.id
def name = responseBodyUser.name
def email = responseBodyUser.email
def addressLine1 = responseBodyUser.address_line1
def addressLine2 = responseBodyUser.address_line2
def town = responseBodyUser.town
def state = responseBodyUser.state
def zip = responseBodyUser.zip
def country = responseBodyUser.country
def phone = responseBodyUser.phone
def role = responseBodyUser.role
def roles = responseBodyUser.roles

//Print response values
println('The user id is :  ' + id)
println('The user name is :  ' + name)
println('The user email is :  ' + email)
println('The user address line 1 is :  ' + addressLine1)
println('The user address line 2 is :  ' + addressLine2)
println('The user town is :  ' + town)
println('The user state is :  ' + state)
println('The user zip is :  ' + zip)
println('The user country is :  ' + country)
println('The user phone is :  ' + phone)
println('The user role is :  ' + role)
println('The user roles is :  ' + roles)

//Database values jenny.maldonado.tx2@gmail.com for user
def idDB = '2'
def nameDB = 'Agent account'
def emailDB = 'jenny.maldonado.tx1@gmail.com'
def addressLine1DB = 'x'
def addressLine2DB = ''
def townDB = 'x'
def stateDB = 'NE'
def zipDB = 'x'
def countryDB = 'USA'
def phoneDB = ''
def roleDB = 'Agent'
def rolesDB = 'Client,Agent'

//Verify response values with the database
WS.verifyElementPropertyValue(responseUser, 'id', idDB)
WS.verifyElementPropertyValue(responseUser, 'name', nameDB)
WS.verifyElementPropertyValue(responseUser, 'email', emailDB)
WS.verifyElementPropertyValue(responseUser, 'address_line1', addressLine1DB)
WS.verifyElementPropertyValue(responseUser, 'address_line2', addressLine2DB)
WS.verifyElementPropertyValue(responseUser, 'town', townDB)
WS.verifyElementPropertyValue(responseUser, 'state', stateDB)
WS.verifyElementPropertyValue(responseUser, 'zip', zipDB)
WS.verifyElementPropertyValue(responseUser, 'country', countryDB)
WS.verifyElementPropertyValue(responseUser, 'phone', phoneDB)
WS.verifyElementPropertyValue(responseUser, 'role', roleDB)
WS.verifyElementPropertyValue(responseUser, 'roles', rolesDB)

