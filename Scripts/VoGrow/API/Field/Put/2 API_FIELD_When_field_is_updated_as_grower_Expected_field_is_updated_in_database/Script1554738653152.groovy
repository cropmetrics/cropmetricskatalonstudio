import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

println('The field id updated is : ' + GlobalVariable.fieldId)

//Print new field values created in field table
def query = 'select id, name, farm_id from cropmetrics.field where id =' + GlobalVariable.fieldId
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def fieldIdCreated = queryResult.getString(1)
	def fieldNameCreated = queryResult.getString(2)
	def farmId = queryResult.getString(3)
	
	println('The field id updated is :' + fieldIdCreated)
	println('The field name updated is :' + fieldNameCreated)
	println('The farm id that contains the field updated is :' + farmId)
	
}

