import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one alid account as client
// 2. Have at least one farm created inthe account of the precondition 1

// Define attribute values for the new field,using the farm id of the precondition 2
def fieldId = '14'
def nameField = ''
def boundaryField = '{"boundaries":[{"boundary":{"coords":[{"x":-101.49568751,"y":37.14976293},{"x":-101.50049403,"y":37.14647907},{"x":-101.50049403,"y":37.14264773},{"x":-101.49723246,"y":37.1411425},{"x":-101.49019435,"y":37.14223722},{"x":-101.48933604,"y":37.14620541},{"x":-101.49294093,"y":37.14866833},{"x":-101.49568751,"y":37.14976293}]}}]}'

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Put field
def requestFieldPut = ((findTestObject('VoGrow/Field/FieldPut', [('access_token') : accessToken, ('fieldId') : fieldId])) as RequestObject)

String bodyRequest = '{"field":{"name":"' + nameField + '","boundary":' + boundaryField + '}}'

try {
	requestFieldPut.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

//Get body response
def responseField = WS.sendRequest(requestFieldPut)

// Get status code
WS.verifyResponseStatusCode(responseField, 400)

