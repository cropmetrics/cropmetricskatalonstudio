import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

/*Preconditions
 1. Have valid account created
 2. Have at least 1 Farm, field created
 3. Have at least 1 probe with valid readings assigned in the same location of the field of the precondition 2
 */
//review the data created previously //
def tokenUserName = 'cropmetrics.agsense@gmail.com'
def tokenPassword = 'cropmetrics'

def famId = '7'
def fieldId = '32'
def startTimeStamp = '1546300800'
def endTimeStamp = '1577750400'

// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

//Get field soil moisture endpoint
requestFieldSMGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldSoilMoisture', [('access_token') : accessToken, ('field_id') : fieldId, ('start_time_stamp') : startTimeStamp, ('end_time_stamp') : endTimeStamp]))

// Get status code
WS.verifyResponseStatusCode(requestFieldSMGet, 200)


