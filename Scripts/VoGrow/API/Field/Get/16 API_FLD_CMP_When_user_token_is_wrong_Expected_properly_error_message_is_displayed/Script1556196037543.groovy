import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm/field created in the account of the precondition 1
// 3. Have other valid account, make sure that the role is only client

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx2@gmail.com'
def tokenPassword = 'cropmetrics'
def fieldIdDB = '2'


// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token


//Get field endpoint
requestFieldGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldGet', [('access_token') : accessToken, ('fieldId') : fieldIdDB]))

// Get status code
//WS.verifyResponseStatusCode(requestFieldGet, 400)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyField = slurper1.parseText(requestFieldGet.getResponseBodyContent())

def messageResponse = responseBodyField.message
def messageExpected = "The user doesn't have permissions to access the field requested."

//Print message response
println('Error message response: ' + messageResponse)
println('Error message expected: ' + messageExpected)

WS.verifyElementPropertyValue(requestFieldGet, 'message', messageExpected)




