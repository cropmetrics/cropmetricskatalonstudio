import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm/field created in the account of the precondition 1

// Get token for the user of the precondition 1
def tokenUserName = 'cropmetrics.grower@gmail.com '
def tokenPassword = 'cropmetrics'


// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get database values
def query = "select up.id, f.id,f.name,fr.id from cropmetrics.field f inner join cropmetrics.farm fr on fr.id = f.farm_id inner join cropmetrics.userprofile up on up.id = fr.userprofile_id where up.username = '" + tokenUserName + "'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {
	userNameIdDB = queryResult.getString(1)
	fieldIdDB = queryResult.getString(2)
	nameFieldDB = queryResult.getString(3)
	farmIdDB = queryResult.getString(4)
}

//Get field endpoint
requestFieldGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldGet', [('access_token') : accessToken, ('fieldId') : fieldIdDB]))

// Get status code
WS.verifyResponseStatusCode(requestFieldGet, 200)