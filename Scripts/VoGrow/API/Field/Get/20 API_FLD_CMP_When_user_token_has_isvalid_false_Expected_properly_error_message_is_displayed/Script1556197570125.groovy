import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm/field created in the account of the precondition 1

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'
def fieldIdDB = '2'


// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token


///////////////////////////
//Connection DB to change the flag to disabled the user
//
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

//Update the user flag to false
def queryUpdate = "UPDATE cropmetrics.userprofile SET isvalid=false WHERE username = '" + tokenUserName + "'"
def queryResult1 = CustomKeywords.'connectionCmDatabase.CmConnection.execute'(queryUpdate)

//////////////////////////

//Get field endpoint
requestFieldGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldGet', [('access_token') : accessToken, ('fieldId') : fieldIdDB]))

// Get status code
//WS.verifyResponseStatusCode(requestFieldGet, 401)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyField = slurper1.parseText(requestFieldGet.getResponseBodyContent())

def messageResponse = responseBodyField.message
def messageExpected = "Authorization has been denied for this request."

//Print message response
println('Error message response: ' + messageResponse)
println('Error message expected: ' + messageExpected)

WS.verifyElementPropertyValue(requestFieldGet, 'message', messageExpected)

//Update the user flag to true
def queryUpdate2 = "UPDATE cropmetrics.userprofile SET isvalid=true WHERE username = '" + tokenUserName + "'"
def queryResult2 = CustomKeywords.'connectionCmDatabase.CmConnection.execute'(queryUpdate2)
