import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// Define attribute values for user of the precondition 1
def dateI = '2019-04-03T00:00:00'
def amountI = '1.2'

// Get tokenfor the user of the precondition 1
def tokenUserName = 'cropmetricsvo.user2@gmail.com'
def tokenPassword = 'cropmetricsvo'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Post new farm for the user of the precondition 1
def requestIrrigationPost = ((findTestObject('VoGrow/Field/FieldPostIrrigation', [('access_token') : accessToken])) as RequestObject)
String bodyRequest = '{"date": "' + dateI + '", "amount": ' +amountI+ '}'

try {
	requestIrrigationPost.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

//Get body response
def responseIrrigation = WS.sendRequest(requestIrrigationPost)

// when the status code is 201
WS.verifyResponseStatusCode(responseIrrigation, 201)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
def IrrigationPostResult = slurper1.parseText(responseIrrigation.getResponseBodyContent())

GlobalVariable.irrigationId = IrrigationPostResult.id


println('New Irrigation created has the next id:  ' + IrrigationPostResult.id)
println('New Irrigation created has the next date:  ' + IrrigationPostResult.date)
println('New Irrigation created has the amount:  ' + IrrigationPostResult.amount)


//WS.verifyElementPropertyValue(responseFarm, 'id', id)