import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm/field created in the account of the precondition 1
// Get tokenfor the user of the precondition 1

def tokenUserName = 'cropmetricsvo.user2@gmail.com'
def tokenPassword = 'cropmetricsvo'

// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get database values
def query = "select ie.id, ie.irrigationdate, ie.amount, ie from cropmetrics.irrigationevent ie inner join cropmetrics.userprofile up on up.id = ie.createdbyuserprofile_id where up.username = '" + tokenUserName + "'"
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next()) {
	idDB = queryResult.getString(1)
	dateDB = queryResult.getString(1)
	amountDB = queryResult.getString(2)
	
}

//Get field endpoint
requestFieldGet = WS.sendRequest(findTestObject('VoGrow/Field/FieldGetIrrigation', [('access_token') : accessToken]))

// Get status code
//WS.verifyResponseStatusCode(requestFieldGet, 200)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyField = slurper1.parseText(requestFieldGet.getResponseBodyContent())

//Print bosy response
println('The id of irrigation:  ' + responseBodyField.irrigationevent.id)
println('The date of irrigation:  ' + responseBodyField.irrigationevent.irrigationdate)
println('The amount of irrigation is:  ' + responseBodyField.irrigationevent.amount)


// Verify body response with the dabatabase data
WS.verifyElementPropertyValue(requestFieldGet, 'id', idDB)
WS.verifyElementPropertyValue(requestFieldGet, 'date', dateDB)
WS.verifyElementPropertyValue(requestFieldGet, 'amount', amountDB)




