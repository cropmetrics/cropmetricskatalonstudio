import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account

// Define attribute values for user of the precondition 1
def name = ''
def farmId = '7'

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx1@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Post new farm for the user of the precondition 1
def requestFarmPut = ((findTestObject('VoGrow/Farm/FarmPut', [('access_token') : accessToken, ('farmId') : farmId])) as RequestObject)
String bodyRequest = '{"name": "' + name + '"}'

try {
	requestFarmPut.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

//Get body response
def responseFarm = WS.sendRequest(requestFarmPut)

// Get the status code
//WS.verifyResponseStatusCode(responseFarm, 400)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
def FarmPutResult = slurper1.parseText(responseFarm.getResponseBodyContent())
def messageResponse = FarmPutResult.message
def messageExpected = "The farm name must be provided."

//Print message response
println('Error message response: ' + messageResponse)
println('Error message expected: ' + messageExpected)

WS.verifyElementPropertyValue(responseFarm, 'message', messageExpected)

