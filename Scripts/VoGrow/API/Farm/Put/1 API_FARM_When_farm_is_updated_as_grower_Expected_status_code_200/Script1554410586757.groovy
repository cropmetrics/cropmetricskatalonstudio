import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client

// Define attribute values for user of the precondition 1
def name = 'Farm 10 katalon Updated'
def farmId = '1'

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx2@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Post new farm for the user of the precondition 1
def requestFarmPut = ((findTestObject('VoGrow/Farm/FarmPut', [('access_token') : accessToken, ('farmId') : farmId])) as RequestObject)
String bodyRequest = '{"name": "' + name + '"}'

try {
    requestFarmPut.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
    println(ex.detailMessage)
} 

//Get body response
def responseFarm = WS.sendRequest(requestFarmPut)

// when the status code is 201
WS.verifyResponseStatusCode(responseFarm, 200)

// Get body response attributes for post farm endpoint
def slurper1 = new groovy.json.JsonSlurper()
def FarmPutResult = slurper1.parseText(responseFarm.getResponseBodyContent())

GlobalVariable.farmId = FarmPutResult.id
GlobalVariable.userId = FarmPutResult.user_id

println('The Farm updated has the next id:  ' + FarmPutResult.id)
println('The Farm updated has the next name:  ' + FarmPutResult.name)
println('The Farm updated has the user id:  ' + FarmPutResult.user_id)

WS.verifyElementPropertyValue(responseFarm, 'id', farmId)
