import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm created in the account of the precondition 1
// 3. Have a valid dealer account, make sure that the user of the precondition 1 is assigned as grower as read only.

// Get tokenfor the user of the precondition 3
def tokenUserName = 'jenny.maldonado.tx.agent.ro@gmail.com'
def tokenPassword = 'cropmetrics'
def growerId = '3'


// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token


//Get Farm Field Users data
requestFarmGet = WS.sendRequest(findTestObject('VoGrow/Farm/FarmGet', [('access_token') : accessToken]))

// Get status code
//WS.verifyResponseStatusCode(requestFarmGet, 200)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyFarm = slurper1.parseText(requestFarmGet.getResponseBodyContent())

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Get database values
def query = 'select id, name, userprofile_id from cropmetrics.farm where userprofile_id = ' + growerId + 'order by  id asc'
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)
def count = 0

while (queryResult.next())
{
	farmIdDB = queryResult.getString(1)
	farmNameDB = queryResult.getString(2)
	growerIdDB = queryResult.getString(3)
	
	//Print bosy response
	println('The farm id of the grower is:  ' + responseBodyFarm[count].id)
	println('The farm name of the grower is:  ' + responseBodyFarm[count].name)
	println('The user id of the growe is:  ' + responseBodyFarm[count].user_id)
	
	// Verify body response with the dabatabase data
	WS.verifyElementPropertyValue(requestFarmGet, '['+count+'].id', farmIdDB)
	WS.verifyElementPropertyValue(requestFarmGet, '['+count+'].name', farmNameDB)
	WS.verifyElementPropertyValue(requestFarmGet, '['+count+'].user_id', growerIdDB)
	
	count = count+1
	println(count)
}
