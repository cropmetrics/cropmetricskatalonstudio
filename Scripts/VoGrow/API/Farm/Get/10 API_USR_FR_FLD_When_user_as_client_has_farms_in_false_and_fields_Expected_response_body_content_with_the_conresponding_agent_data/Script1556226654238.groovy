import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account as client
// 2. Have at least one farm with at least two fields created in the account of the precondition 1

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx2@gmail.com'
def tokenPassword = 'cropmetrics'

// Field Id of one field of the precondition 2
def farmId = '1'

// Post token
responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

// Update the flag isvalid to false for one of the fields of the precondition 2
def queryUpdateFarm = 'UPDATE cropmetrics.farm SET isvalid=false WHERE id = ' + farmId
def queryResultFarmFlag = CustomKeywords.'connectionCmDatabase.CmConnection.execute'(queryUpdateFarm)

//Get Farm Field Users data
requestFarmGet = WS.sendRequest(findTestObject('VoGrow/Farm/FarmGet', [('access_token') : accessToken]))

// Get status code
//WS.verifyResponseStatusCode(requestFarmGet, 200)

// Get body response attributes for put field endpoint
def slurper1 = new groovy.json.JsonSlurper()
def responseBodyFarm = slurper1.parseText(requestFarmGet.getResponseBodyContent())

//Print bosy response
println('The get farm body response is:  ' + responseBodyFarm)
def getFarmResponse = 'null'
	
// Verify body response with the dabatabase data
WS.verifyElementPropertyValue(requestFarmGet,'[0]', getFarmResponse)


// Update the flag isvalid to false for one of the fields of the precondition 2
def queryUpdateField2 = 'UPDATE cropmetrics.farm SET isvalid=true WHERE id = ' + farmId
def queryResultFieldFlag2 = CustomKeywords.'connectionCmDatabase.CmConnection.execute'(queryUpdateField2)


