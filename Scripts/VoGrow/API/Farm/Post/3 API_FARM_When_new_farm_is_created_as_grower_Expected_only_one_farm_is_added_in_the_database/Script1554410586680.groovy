import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Connection DB
CustomKeywords.'connectionCmDatabase.CmConnection.connectDB'('localhost', 'virtualoptimizer', '5432', 'postgres', 'admin')

//Print new farm values created in farm table
def query = 'select count(userprofile_id), userprofile_id from cropmetrics.farm where userprofile_id =' + GlobalVariable.userId + ' and name =' + GlobalVariable.farmName + ' group by userprofile_id'
def queryResult = CustomKeywords.'connectionCmDatabase.CmConnection.executeQuery'(query)

while (queryResult.next())
{
	def nroFarmIdCreated = queryResult.getString(1)
	def userprofileId = queryResult.getString(2)
	
	
	println('The number of the farm ids created is/are :' + nroFarmIdCreated)
	println('The user profile id of the farms created is :' + userprofileId)
	
	//if(nroFarmIdCreated > 1)
	//{
		//Pintln('ERROR in the creation existe more than 1 farm created')
	//}
	
}