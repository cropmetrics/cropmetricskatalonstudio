import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ResponseObject as ResponseObject
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent

// Preconditions
// 1. Have at least one valid account
// 2. Have other valid account

// Define attribute values for user of the precondition 2
def name = 'New Farm with userID different of the user token'
def userId = '4' 

// Get tokenfor the user of the precondition 1
def tokenUserName = 'jenny.maldonado.tx@gmail.com'
def tokenPassword = 'cropmetrics'

responseToken = WS.sendRequest(findTestObject('VoPro/Token', [('usernameToken') : tokenUserName, ('passwordToken') : tokenPassword]))

// Get body response attributes for the token
def slurper = new groovy.json.JsonSlurper()
def tokenResult = slurper.parseText(responseToken.getResponseBodyContent())

// Get result parameters of token
def accessToken = tokenResult.access_token

println('.. Current access token generated for admin user : ' + accessToken)

// Post new farm for the user of the precondition 1
def requestFarmPost = ((findTestObject('VoGrow/Farm/FarmPost', [('access_token') : accessToken])) as RequestObject)
String bodyRequest = '{"name": "' + name + '", "user_id": "' +userId+ '"}'

try {
	requestFarmPost.setBodyContent(new HttpTextBodyContent(bodyRequest, 'UTF-8', 'application/json'))
}
catch (Exception ex) {
	println(ex.detailMessage)
}

//Get body response
def responseFarm = WS.sendRequest(requestFarmPost)

//Get status code of farm post endpoint
WS.verifyResponseStatusCode(responseFarm, 400)




