<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RainBucketReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fac05713-a9e2-402b-a67a-59ea61947ab9</testSuiteGuid>
   <testCaseLink>
      <guid>f0bfbe76-4cd9-47f0-a793-f6c2955041d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/RainBucketReadings/RMQ_CPJR_RBR_When_agsense_probe_has_valid_readings_with_currain_value_more_than_0_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3963ef8c-ed97-473c-9623-2b359a1541d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/RainBucketReadings/RMQ_CPJR_RBR_When_agsense_probe_has_valid_readings_with_currain_value_Expected_new_record_is_be_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
