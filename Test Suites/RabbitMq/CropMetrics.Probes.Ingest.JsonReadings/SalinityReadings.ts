<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SalinityReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c1bdd143-6fb4-410e-a797-0b6f6cd65325</testSuiteGuid>
   <testCaseLink>
      <guid>9f86debb-d0fa-48c9-9452-941321f81e32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SalinityReadings/RMQ_CPJR_SR_When_agsense_probe_has_valid_readings_with_salinity_value_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2edbbdce-1dc1-4e03-a08f-ddfd35d2a2cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SalinityReadings/RMQ_CPJR_SR_When_agsense_probe_has_valid_readings_with_salinity_value_Expected_new_record_is_be_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
