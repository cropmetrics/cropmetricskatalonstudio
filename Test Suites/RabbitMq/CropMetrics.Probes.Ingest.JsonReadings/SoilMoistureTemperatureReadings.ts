<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SoilMoistureTemperatureReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ed6f0411-f57f-4b6c-8548-fd06de832916</testSuiteGuid>
   <testCaseLink>
      <guid>a62a07ff-ca55-42c5-ada3-302695db1608</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SMTemperatureReadings/RMQ_CPJR_SMTR_When_agsense_probe_has_temperature_readings_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5eee30b-d4b6-42c2-a7ea-dea50d5ad278</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SMTemperatureReadings/RMQ_CPJR_SMTR_When_agsense_probe_has_temperature_readings_Expected_new_record_is_added_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
