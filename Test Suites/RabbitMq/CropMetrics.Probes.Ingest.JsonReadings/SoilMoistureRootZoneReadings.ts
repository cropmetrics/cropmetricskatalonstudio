<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SoilMoistureRootZoneReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d6e2bab4-53ed-409e-a932-39c32b62dba1</testSuiteGuid>
   <testCaseLink>
      <guid>369c00ce-5478-4f84-b28b-1f38e0f9c13c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SoilMoistureRootZoneReadings/RMQ_CPJR_SMRZR_When_agsense_probe_has_valid_readings_and_root_zone_defines_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8c7d0e9-aafb-4174-9f4e-41274c841568</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/SoilMoistureRootZoneReadings/RMQ_CPJR_SMRZR_When_ag_probe_has_valid_readings_and_root_zone_defined_Expected_new_record_is_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
