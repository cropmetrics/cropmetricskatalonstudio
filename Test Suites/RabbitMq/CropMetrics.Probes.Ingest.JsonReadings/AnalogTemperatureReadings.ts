<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AnalogTemperatureReadings</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6e1de163-24b7-4916-baed-1106d1e6e5b4</testSuiteGuid>
   <testCaseLink>
      <guid>7bb2c6ef-915f-460f-871f-8d57a7ab8676</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/AnalogTemperatureReadings/RMQ_CPJR_ATR_When_agsense_probe_has_valid_readings_with_temp5_value_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea23d6ee-63eb-48e5-9f82-981e3b1630e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RabbitMq/JsonReadings/AnalogTemperatureReadings/RMQ_CPJR_ATR_When_agsense_probe_has_valid_readings_with_temp5_value_Expected_new_record_is_be_in_database_with_correct_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
