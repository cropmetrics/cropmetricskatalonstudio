<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manual_Iteris_Weather_DailyHistorical</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f6f5090e-ffc2-44df-a951-566cacaadffe</testSuiteGuid>
   <testCaseLink>
      <guid>8e2dd4ab-7087-4925-a9c9-fb34b907badf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_custom_hourtorun_and_minutestorun_values_Expected_process_start_according_values_configured_in_server_time</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8d5aadf5-9bd6-4010-bfb9-fc41ae6ef71f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7c01e4e5-2cc0-413c-928e-bf2ab81ee143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_field_does_not_exist_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>95f39003-2059-436b-b5aa-95a65fa4fba7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1d1e56a3-adcd-4c8f-8c06-4a891c523b7a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0fda7995-7b9b-4477-a071-b510085a51a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_field_flag_isvalid_is_false_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>32c77c18-efe7-4ebe-8c9b-92b96f22e99b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7e81d23a-2621-4aff-ae57-d5e36680706d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f8ee6cac-1651-42db-ae76-65f0dfa06dc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_field_flag_isvalid_is_false_Expected_field_does_not_genereate_any_message</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>dc48cc05-525e-4bcd-8288-61eb7008d5c6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fc3e2eac-3ce8-4224-b90c-2de09f684ded</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0d980c25-482b-4bd4-9600-46a08824bace</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_field_is_char_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>917e16fa-c927-4fb2-88ff-cc1009429991</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>183ca08b-69b7-4454-adfc-d526657a8da0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c908ca44-dbd5-4b0a-a83d-d0b9a0745958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_field_parameter_is_empty_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>5cf258f5-2139-4e3f-8274-b1551304faae</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c82d43b6-b1a7-45a1-9958-e5b103ce35bc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>43951d1e-c8de-470a-bc00-f524f7c19909</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_1_valid_field_id_Expected_new_row_should_store_in_dailyhistorical_table_of_iterisrawdata_database</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7f7e1ebb-f832-4a57-a0dc-034fdfffea8f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2bd9f802-0303-4fbd-a199-3d4680512a04</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>16eacdda-930b-44ef-a8c1-d0e2df42611c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_5_field_identifiers_are_grouped_with_the_same_time_zone_Expected_message_per_field_id_is_generated</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>64aa686e-8e0d-4d01-9049-53b8e83cb0b0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>946dd78b-3ab4-44d7-84ea-e8ce1332c334</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e4d8c30c-304b-4435-bb37-5484e5e061a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_5_valid_fields_in_different_time_zone_Expected_error_message_should_generate_in__log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6bd6a33d-c21f-4847-a519-63d4ca18e817</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d70da639-db64-4945-b075-49f50947600b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>942eed72-4897-4a79-a0db-3a569bcb3a28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_5_valid_fields_in_the_same_time_zone_Expected_new_row_should_be_stored_in_the_database_per_field</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c74dba82-4288-4078-a451-dd038cf70de2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>937e8466-9f19-4b98-8762-439f9cb0f070</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d672636b-a0e8-41a9-bfd9-1feee5c250c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_field_identifiers_in_different_time_zone_Expected_message_per_field_id_is_generated</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2c41f98f-b810-41a4-9418-ddc7658bb498</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c67c9064-849d-4e13-8c95-2107623d2ab0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>044ed253-053e-4409-b835-b573e869e47e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_more_than_5_valid_fields_Expected_error_message_should_generate_in_log_files</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>575e5abf-82a5-4ecc-9048-50b8c514875d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d958f4f9-1a77-48dc-80a8-771ed17186b5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>eec261ac-985e-481b-8dae-1982fb68b2ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_valid_field_Expected_all_data_should_overwriting</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9102078d-8e73-4b7a-b9e4-5bbfc820bd01</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>618b6826-a8df-44db-b0d4-8e3a6839d344</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4c912927-d38f-445d-8038-bdb7c0bbc852</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_valid_field_Expected_data_stored_should_the_same_of_Iteris_endpoint_daily_historical_v1.2</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>18232f46-1f45-43a4-b825-75a9d809fe4e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>abe50608-6336-4c32-8d59-483e5c1197e6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>732fdc2f-d830-4c67-a49e-8749ae84513e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_valid_field_Expected_data_stored_should_end_at_yesterday_of_current_day</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>36e94142-6398-43ba-a291-2461bc97ab4d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>da1d9232-0372-4935-b463-0a169983d347</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>67b7246e-45a3-40af-bbb6-81b62954c20c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_have_valid_field_Expected_data_stored_should_start_at_january_1_of_current_year_</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>a1dbe2b0-4c17-43e1-b85e-6b2a6566a3f7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4933b2da-8e4a-4847-8df6-87c2b4483d37</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bca5666b-9bf4-4a9f-8e72-e0f033aeaf97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_start_every_publishafterminutes_configured_and_have_new_field_Expected_message_is_generated_only_for_new_field</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>50f770ee-fac6-486f-8543-2c5750ee8eec</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>702e3e62-15f6-4a89-974e-ae709034be2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_time_zone_is_less_than_3_am_Expected_field_is_not_consider_to_generat_a_messege</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c2ae51d5-da42-49a9-b5e1-a2024c8044a5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>22499bf9-7223-41cd-88a5-edf38d620070</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ffa45484-c82c-4005-9e3d-be1722d62ef5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/DataIngest/Iteris/Weather/Historical/DailyHistorical/DI_W_DH_When_time_zone_is_more_than_3_am_Expected_field_is_consider_to_generat_a_messege</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>94141c60-8070-447a-9423-96940ce8418f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/DB_Fields_WeatherDailyHistorical_WeatherDailyForecast</testDataId>
      </testDataLink>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6133e193-66bd-44f4-8dc1-a04cbe06642b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/DataIngest/Iteris/Weather/Historical/DailyHistorical/Xls_Iteris_WeatherDailyHistorical_RabbitMq_Messages</testDataId>
      </testDataLink>
   </testCaseLink>
</TestSuiteEntity>
