<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Manual_Admin_New_Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7effb1b2-a2f5-4890-9c87-5d6cf40d12bb</testSuiteGuid>
   <testCaseLink>
      <guid>8cf7eb5d-67d5-4a25-a84a-18473ea0e58d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_existing_email_Expected_error_message_should_be_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96000ab5-caf0-4ace-8f73-ced8688d392f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_all_fields_are_filled_and_click_on_Clear_button_Expected_all_fields_should_be_clean_except_roles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3de070f6-0e96-4293-a76e-36af274ac3e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_is_created_Expected_can_get_longin_to_vo_website_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3958613f-37d8-4012-88cb-a8e63cb053b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_is_created_Expected_email_notification_content_should_be_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f674ed77-f4b9-475a-a47a-807bf41a9742</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_is_created_Expected_email_notification_should_sent_to_the_email_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14f1809e-9569-4083-89f7-9ef6b3939e3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_is_created_Expected_new_account_should_be_add_properly_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1f8b09a-c7fa-42d3-a4b9-61d27ce4bdbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_tab_is_dispplayed_Expected_username_field_should_be_disapperas</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60a601a1-2a8b-45e5-a33d-cdf3037da357</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_without_all_roles_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>143573bf-4c26-4487-b411-dc11a3c0488d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_with_administrator_role_Expected_new_account_is_created_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39e59717-9183-4e7d-8d80-68054752f3a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_with_agent_role_Expected_new_account_is_created_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>081b35c8-2bc5-4095-b8b3-dd23be4e9b75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_with_all_roles_Expected_new_account_is_created_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b93948e6-a4ba-4b9c-adf3-ef3cb8bd4c8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_with_client_role_Expected_new_account_is_created_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0183677f-f92a-4f58-9c48-2cf9fd8ec64c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_new_account_with_fieldcrew_role_Expected_new_account_is_created_properly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c47394c2-fc7d-44a1-95e9-e9ef3195d3cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_different_values_in_password_and_password(again)_Expected_add_account_button_should_not_be_enabled_</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f6d1b1b-4726-4d5f-89e1-f47023ca0732</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_enabled_if_all_required_values_are_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25c539fc-a6a2-4fdc-8e10-950368002326</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_address_line_1_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c22df5d-809e-4d4b-b243-2b4313ede99f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_country_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36573b70-8ade-47a2-a68b-b64df097a69e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_email_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ac9382d-2b04-4689-9aed-8c32b560b6db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_password(again)_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9573f4ed-c574-49cd-90e0-d777b4823773</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_password_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82b2ad59-3950-4939-a008-a75762730cae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_real_name_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2395df95-7ee3-460b-8807-21afba8423ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_roles_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e01e8ebe-69a1-4978-bcba-111645bf0fc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_state_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c5c7d25-5666-4715-a883-297425b7227a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_town_city_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2792bac-355e-4779-857e-094ae4534c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_add_account_button_should_not_enabled_if_zip_is_not_filled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>964fd926-72b6-4c09-8be0-0d6d6963fd25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_Expected_role_by_default_should_be_client</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46e70fe7-dcbf-4831-9a1f-692b6df27f26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_empty_email_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adc5e61d-b16f-4e51-b881-bd29b0b774ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_empty_password(Again)_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e696cb2-f66a-486a-837d-2755eec6a7bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_empty_password_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88a88ab3-924e-4f4a-8360-cd4bbb91b2fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_less_6_chars_as_password(Again)_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c103bf01-b2b3-45ab-8b28-180b2c7ec08c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_less_6_chars_as_password_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7fc9279-e289-4022-a1d5-5510c08307e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_only_spaces_as_password(Again)_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6edd26f-e781-4d77-9ca8-8d76556cb2d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_only_spaces_as_password_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f06d311-2ca8-4c3f-8907-1632a4f2d99a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_spaces_in_the_email_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d2a3b99-82bc-42be-bbf1-ed6026096550</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_symbols_as_email_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b13b3e8b-5ab9-4db7-ae63-96ed1a51c99a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoPro/UI/Admin/NewAccount/UI_ADM_ACNT_When_try_to_create_new_account_with_wrong_email_format_Expected_add_account_button_should_not_be_enabled</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
