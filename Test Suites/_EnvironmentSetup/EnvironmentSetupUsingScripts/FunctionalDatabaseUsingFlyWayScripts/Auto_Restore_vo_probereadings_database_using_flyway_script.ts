<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_probereadings_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>35093b49-49bc-49ac-89d2-73a3dcbfa08e</testSuiteGuid>
   <testCaseLink>
      <guid>147695a9-9851-4ab7-ab0e-82d461cf8b2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_probereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>478784fe-6f8f-4b3a-849a-0e0066320ac7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_vo_probereadings_with_flyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
