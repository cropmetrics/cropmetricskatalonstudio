<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_iterisrawdata_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>dee36832-8f08-44f7-bbcc-52c062bf2924</testSuiteGuid>
   <testCaseLink>
      <guid>4a2f04c9-71bf-4be2-966c-3d17b2f9de06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_iterisrawdata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d44a6a95-8e39-4742-8aba-8069675e375e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_iterisrawdata_with_fkyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
