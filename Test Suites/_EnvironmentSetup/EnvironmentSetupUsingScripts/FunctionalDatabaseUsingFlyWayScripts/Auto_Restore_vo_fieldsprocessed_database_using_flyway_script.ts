<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_fieldsprocessed_database_using_flyway_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f0e3c751-bfb6-425e-bcfe-03e28b540360</testSuiteGuid>
   <testCaseLink>
      <guid>aa835105-bfb0-4485-b00a-27060642fc54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_fieldsProcessed_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da49f227-3e6e-4ad8-9d9b-149c731ad194</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/Functional/DB_vo_fieldsProcessed_with_fkyway</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
