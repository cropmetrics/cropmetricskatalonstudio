<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_sentekproberawreadings_database_using_sql_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f053a5d4-9bfb-4572-b9b4-5ddd30654b8a</testSuiteGuid>
   <testCaseLink>
      <guid>97c031b9-0115-456f-b3d5-cba400f12fc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_sentekproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4df245e1-7fde-44e6-bc05-12eca401e6a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/ProbeReadings/DB_vo_sentekproberawreadings_wiht_flyway</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c6165b2-b375-43ad-b7fb-27b8833802d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_sentekproberawreadings_with_sql_script_for_schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>480904a0-cfe0-4939-b38c-5d9cf3d7aa3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_sentekproberawreadings_with_sql_script_for_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
