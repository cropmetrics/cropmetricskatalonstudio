<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_sentekprobereadings_database_using_sql_script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fa43d429-b10b-4e40-b473-1e4fc336b5a8</testSuiteGuid>
   <testCaseLink>
      <guid>426bbd80-2c94-4ab4-b9c5-cc36be049495</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_sentekprobereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2413573f-0f13-420f-a927-7a70a7784388</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesWithFlyway/ProbeReadings/DB_vo_sentekprobereadings_wiht_flyway</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08c9c2c0-98ed-4a7e-bc94-1a0afea0ff01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_sentekprobereadings_with_sql_script_for_schema</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e08f54e-cad1-4fdf-8612-a3e5690d142c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/ProbeReadingsRestoreWithSqlScripts/DB_vo_sentekprobereadings_with_sql_script_for_data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
