<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_iterisrawdata_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>87df0d81-550d-42ae-8b3c-247fa2bb4267</testSuiteGuid>
   <testCaseLink>
      <guid>e990ccd6-3252-4485-9743-0562c9bd3589</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_iterisrawdata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>955d61fa-3ca7-4094-a04e-b71f45dd21f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_iterisrawdata_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
