<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_virtualoptimizer_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e6c4de1e-ed81-4656-9441-63190e0fd93a</testSuiteGuid>
   <testCaseLink>
      <guid>2856aa08-b132-4e4a-ba8f-e546dfb2ccb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_virtualoptimizer_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac6aa193-c1f7-4c8e-b84a-a7c1631f4abc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_virtualoptimizer_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
