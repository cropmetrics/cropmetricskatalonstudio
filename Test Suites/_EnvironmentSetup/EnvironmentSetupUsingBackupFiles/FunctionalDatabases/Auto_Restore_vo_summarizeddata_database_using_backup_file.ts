<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_summarizeddata_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae1890bb-a034-437a-b8e2-bb59abc547e4</testSuiteGuid>
   <testCaseLink>
      <guid>0644f8d6-caef-45d2-9d56-d523fbb70dc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_summarizeddata_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee77a5d9-ee88-4e01-ab0c-0fb405fdc20c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_vo_summarizeddata_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
