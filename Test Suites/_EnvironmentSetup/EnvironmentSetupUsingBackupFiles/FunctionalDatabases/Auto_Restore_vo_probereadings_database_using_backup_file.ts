<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_probereadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7d9a6570-4750-4e5c-9067-5204e4cbf7e0</testSuiteGuid>
   <testCaseLink>
      <guid>63177893-c515-499f-b597-e51cc0b66971</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/Functional/DB_Create_vo_probereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51b23af5-0c37-4529-a9fc-f35ad27d206d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/Functional/DB_Restore_vo_probereadings_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
