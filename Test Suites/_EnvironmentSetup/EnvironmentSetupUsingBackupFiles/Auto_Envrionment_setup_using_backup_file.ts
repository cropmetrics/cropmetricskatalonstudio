<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteCollectionEntity>
   <description></description>
   <name>Auto_Envrionment_setup_using_backup_file</name>
   <tag></tag>
   <executionMode>SEQUENTIAL</executionMode>
   <maxConcurrentInstances>8</maxConcurrentInstances>
   <testSuiteRunConfigurations>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_virtualoptimizer_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_iterisrawdata_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_vo_probereadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_vo_ssurgoRawData_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_vo_summarizeddata_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/FunctionalDatabases/Auto_Restore_vo_fieldsprocessed_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/ProbeReadingsDatabases/Auto_Restore_vo_agsenseproberawreadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/ProbeReadingsDatabases/Auto_Restore_vo_agsenseprobereadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/ProbeReadingsDatabases/Auto_Restore_vo_loraproberawreadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/ProbeReadingsDatabases/Auto_Restore_vo_sentekproberawreadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
      <TestSuiteRunConfiguration>
         <configuration>
            <groupName>Web Desktop</groupName>
            <profileName>default</profileName>
            <runConfigurationId>Chrome</runConfigurationId>
         </configuration>
         <runEnabled>true</runEnabled>
         <testSuiteEntity>Test Suites/_EnvironmentSetup/EnvironmentSetupUsingBackupFiles/ProbeReadingsDatabases/Auto_Restore_vo_sentekprobereadings_database_using_backup_file</testSuiteEntity>
      </TestSuiteRunConfiguration>
   </testSuiteRunConfigurations>
</TestSuiteCollectionEntity>
