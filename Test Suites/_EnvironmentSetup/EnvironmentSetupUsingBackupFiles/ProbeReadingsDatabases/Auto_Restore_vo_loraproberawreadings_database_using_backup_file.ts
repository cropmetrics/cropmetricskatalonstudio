<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_loraproberawreadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>33d5629b-5d35-4b06-abab-fb42455897bd</testSuiteGuid>
   <testCaseLink>
      <guid>c076d7f1-06ee-4e09-9358-60e14505e13d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_loraproberawreadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49b82948-51ab-49c9-9ea7-fa9769e16b30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/ProbeReadings/DB_Restore_vo_loraproberawreadings_with_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
