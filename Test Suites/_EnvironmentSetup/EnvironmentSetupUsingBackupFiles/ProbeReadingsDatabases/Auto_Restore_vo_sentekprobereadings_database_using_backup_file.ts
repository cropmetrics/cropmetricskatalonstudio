<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Restore_vo_sentekprobereadings_database_using_backup_file</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c92d2bee-d3ca-44cf-8f11-438a903b2f9f</testSuiteGuid>
   <testCaseLink>
      <guid>d91f38e1-d993-41c3-a138-9d3c7debc6d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/CreateDatabases/ProbeReadings/DB_Create_vo_sentekprobereadings_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7c8f399-307f-48d2-bca8-174a0b82255e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Databases/DatabasesRestoreWithBakcupFiles/ProbeReadings/DB_Restore_vo_sentekprobereadings_wiht_backup_file</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
