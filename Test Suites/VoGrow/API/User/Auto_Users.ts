<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Users</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5ebdc484-f0c6-4956-a4fb-076a5933f4ff</testSuiteGuid>
   <testCaseLink>
      <guid>79ee32bb-6be4-4490-8949-f53e0f3d185b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/1 API_USERS_When_user_is_client_only_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>703e3f53-1f52-4b29-a780-64579916f5d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/2 API_USERS_When_user_is_client_only_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd6d00b2-ce3e-46f8-98fd-78ead80f4ca7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/3 API_USERS_When_user_is_agent_only_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>827ea8d9-6aea-4d28-9cd9-6ac81b1f7b39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/4 API_USERS_When_user_is_agent_only_Expected_response_body_content_with_the_conresponding_agent_data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63d22963-f4d9-4086-81b9-08e663ef759b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/5 API_USERS_When_user_token_is_wrong_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf492b3d-6bee-4b68-bfc1-9e848f0e4cfb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/6 API_USERS_When_user_token_is_wrong_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f804fe0-4a0c-41cc-b9b8-6461df29151e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/7 API_USERS_When_user_has_admin_role_Expected_status_code_401</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>979ea0d2-5bb0-4ed5-a151-98634a060ffd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/8 API_USERS_When_user_has_admin_role_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1fe575a-be69-40fc-868d-2642ada6e004</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/9 API_USERS_When_user_is_agent_with_grower_disabled_Expected_status_code_200</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f8aedf4-3c72-4dd8-9c67-125c7d549978</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/User/InfoDealer/10 API_USERS_When_user_is_agent_with_grower_disabled_Expected_response_body_content_with_the_agent_data_only</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
