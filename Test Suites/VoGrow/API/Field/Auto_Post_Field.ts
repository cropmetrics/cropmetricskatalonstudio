<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Auto_Post_Field</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>37222a4e-bd86-4db3-ad17-b68ee574bffd</testSuiteGuid>
   <testCaseLink>
      <guid>e700e018-de3c-469a-a15a-e5ab29073829</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/1 API_FIELD_When_field_is_created_as_grower_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>efbb4d0f-d934-4bac-a8f3-632ab95a771c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/2 API_FIELD_When_new_field_is_created_as_grower_Expected_new_field_is_added_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ced0911e-25a2-4b7c-80dc-a35cc20a2c24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/3 API_FIELD_When_new_field_is_created_as_grower_Expected_only_one_field_is_added_in_the_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6215ead0-11dd-40ae-834e-0bc31b10ece0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/4 API_FIELD_When_new_field_is_created_as_dealer_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb46a791-de27-414a-8138-4ee1d8175372</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/5 API_FIELD_When_new_field_as_dealer_for_grower_not_assigned_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a371972a-b03b-46a6-a6b9-4c33491f94f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/6 API_FIELD_When_field_created_as_dealer_for_grower_not_assigned_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3952204-3c28-4e8a-b4b0-935d39590194</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/7 API_FIELD_When_new_field_as_dealer_for_one_grower_assigned_Expected_new_field_is_added_in_database_only_for_corresponding_farm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00793d42-6b90-4693-afef-17f5ce595ddc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/9 API_FIELD_When_new_field_has_the_farmid_attribute_as_empty_Expected_status_code_400</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d6c0bf4a-4def-422b-9403-29daa90d7ae8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/VoGrow/DB_2Agent_2Growers</testDataId>
      </testDataLink>
   </testCaseLink>
   <testCaseLink>
      <guid>839c277b-d3ca-4dd5-9f78-dd9d3d97d135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/10 API_FIELD_When_new_field_has_the_farmid_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6f29b24-0b78-43db-a15d-3d35a44cf714</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/11 API_FIELD_When_new_field_has_the_farmid_attribute_with_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf931509-7f1c-4ba6-abc7-5aac00111b64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/12 API_FIELD_When_new_field_has_the_farmid_attribute_with_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22d20ca5-5303-4d94-8721-43328363ce0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/13 API_FIELD_When_new_field_has_the_farmid_attribute_with_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>842607b8-e3ed-437d-a8b3-d238c38f1eb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/14 API_FIELD_When_new_field_has_the_farmid_attribute_with_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fceb6064-873f-4868-b98a-105ad65e0a2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/15 API_FIELD_When_new_field_has_the_farmid_attribute_with_symbols_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>249fd282-72b5-4e20-90c1-d602b3c036c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/16 API_FIELD_When_new_field_has_the_farmid_attribute_with_symbols_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90ce36c7-abf0-4466-8766-a707eae3f62e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/17 API_FIELD_When_new_field_has_the_valid_farmid_attribute_but_different_of_the_user_token_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e806bdc-f7fa-44b3-9f96-dab623579ad5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/18 API_FIELD_When_new_field_has_valid_farmid_attribute_but_different_of_user_token_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d141f48-9df0-4400-8adf-6527e44e8860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/19 API_FIELD_When_new_field_has_an_invalid_farmid_attribute_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>025f5d08-466d-4778-9ef9-2403cb87047b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/20 API_FIELD_When_new_field_has_an_invalid_farmid_attribute_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af24fd9e-31f9-46a9-8e12-77d07a7da058</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/21 API_FIELD_When_new_field_has_name_attribute_with_more_than_128_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>725abdce-5a4b-4413-8b23-3e25ba9f54f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/22 API_FIELD_When_new_field_has_name_attribute_with_more_than_128_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b1caebf-11a5-4b1e-a203-465a8f6746f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/23 API_FIELD_When_new_field_has_name_attribute_with_128_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c947edc-f7cd-41d3-90e7-9b2d5a4895c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/24 API_FIELD_When_new_field_has_name_attribute_with_128_chars_Expected_new_field_is_added_in_database</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b179723b-e478-48d9-a632-f0fce8579695</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/25 API_FIELD_When_new_field_has_name_attribute_with_only_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ce2913c-fb4a-498e-ace3-ab4db2ecc163</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/26 API_FIELD_When_new_field_has_name_attribute_with_only_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f28ab0a-adfb-45dc-b43d-61c3603f8f4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/27 API_FIELD_When_new_field_has_name_attribute_as_empty_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23905112-38c7-46f6-9eb6-7e6f660bd745</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/28 API_FIELD_When_new_field_has_name_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67c26aae-bf6c-4957-a510-17dee953e829</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/29 API_FIELD_When_new_field_has_name_attribute_as_symbols_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c01d27c4-cc9f-4026-b629-c06ff588f89e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/30 API_FIELD_When_new_field_has_name_attribute_as_symbols_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0249e29-4bd1-4f2f-bf24-5d472022941f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/31 API_FIELD_When_new_field_has_name_attribute_with_spaces_and_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dffd082-b3d4-476b-9438-429a36a4993e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/32 API_FIELD_When_new_field_has_name_attribute_with_apostrophes_and_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d082d047-11b5-4969-b225-be3d2d9336c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/33 API_FIELD_When_new_field_has_name_attribute_with_only_numbers_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87c76af1-becd-40ea-a317-d3106d97bcff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/34 API_FIELD_When_new_field_has_name_attribute_with_only_chars_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86e75abf-7f59-4bfe-8ac4-4caabfcf29bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/35 API_FIELD_When_new_field_has_an_existing_name_in_farm_id_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7eb05446-c7c8-4585-bc5e-d13f39eceaf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/36 API_FIELD_When_new_field_has_an_existing_name_in_farm_id_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4013a21-e7de-4c72-bfc4-dc3265f6d892</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/37 API_FIELD_When_new_field_has_the_name_of_the_other_farm_id_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c763a6e-ae46-4c93-acf9-c782658b75d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/38 API_FIELD_When_new_field_is_created_as_dealer_read_only_for_one_of_the_grower_assigned_Expected_status_code_201</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98863662-c53f-48ce-99ca-ea54981ba6c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/39 API_FIELD_When_new_field_is_created_into_farm_with_flag_isvalid_false_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeb73480-e67e-4506-a728-627c31a24f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/40 API_FIELD_When_new_field_is_created_into_farm_with_flag_isvalid_false_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71cf3187-4acd-4c24-9a59-44dff8e6d3b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/41 API_FIELD_When_new_field_is_created_with_boundary_attribute_as_empty_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8bb822b-ad68-4d42-be26-140ab96045fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/42 API_FIELD_When_new_field_is_created_with_boundary_attribute_as_empty_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ef5e3c4-fe2c-4261-991f-d7c071e73bb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/43 API_FIELD_When_new_field_is_created_with_boundary_attribute_as_chars_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0cc4e12-8b2e-4eec-b291-6dfa73514b01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/44 API_FIELD_When_new_field_is_created_with_boundary_attribute_as_chars_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05742393-c5b2-460f-80d6-b70ab6d88b95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/45 API_FIELD_When_new_field_is_created_with_boundary_attribute_with_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad913ecb-4f53-4565-b609-ed55dd6a57cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/46 API_FIELD_When_new_field_is_created_with_boundary_attribute_with_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c39ff66-2073-4f48-a459-8a3cd96cb733</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/47 API_FIELD_When_new_field_is_created_with_boundary_attribute_without_coords_values_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96b74dc4-fe15-49eb-b7af-e02360b8eec6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/48 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_coords_values_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6669225c-aea1-40c8-81b4-2f3330ead3ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/49 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_invalid_x_value_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff12b331-f148-48bd-b1b6-91f6f12676db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/50 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_invalid_x_value_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b8f8af3-0108-493d-985c-e5f8f117d924</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/51 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_invalid_y_values_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13a42c53-3718-434b-b234-a414ed40ae69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/52 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_invalid_y_values_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69fc3025-e9d5-4853-bfba-bf292eceb6b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/53 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_spaces_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e41b45c7-06a1-4571-a612-94dabcf6ebd7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/54 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_with_spaces_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f746b75-d989-478e-be1c-7b4c18f997ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/55 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_x_values_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2727950e-db49-4b25-881b-f3e0143f8009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/56 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_x_values_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e1d8505-941f-47b6-8258-3d1f78397a08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/57 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_y_values_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7bd06714-5bdb-444a-a37e-3da543989129</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/58 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_y_values_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa69fe49-86d9-43e7-8879-6da0626c3f41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/59 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_one_point_Expected_status_code_400</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13b78f58-222e-4ea3-ad48-9e6e65294d4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/VoGrow/API/Field/Post/60 API_FIELD_When_new_fiel_is_created_with_boundary_attribute_without_one_point_Expected_properly_error_message_is_displayed</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
